{pkgs, ...}:
pkgs.stdenv.mkDerivation rec {
  pname = "theme-adw-gtk3";
  version = "3.3";
  src = pkgs.fetchurl {
    url = "https://github.com/lassekongo83/adw-gtk3/releases/download/v3.3/adw-gtk3v3-3.tar.xz";
    sha256 = "JvC8ZHb4Vd9ihRAm98ILt+ZLxLiEVxQ20yZNNCFPNGg=";
  };

  sourceRoot = ".";
  installPhase = ''
    dest=$out/share/themes
    mkdir -p $dest
    cp -r -v adw-gtk3 $dest
    cp -r -v adw-gtk3-dark $dest
  '';
}
