{
  pkgs,
  home-manager,
  ...
}: let
  inherit (pkgs) lib system stdenv;
  concatOrUpdate = x:
    builtins.foldl' (a: b:
      if builtins.isNull a
      then b
      else if builtins.isNull b
      then a
      else if builtins.isAttrs a
      then lib.attrsets.recursiveUpdate a b
      else if builtins.isList a
      then a ++ b
      else b)
    null
    x;
in {
  mkHome = hosts: config:
    lib.attrsets.genAttrs hosts
    (hostName: home-manager.lib.homeManagerConfiguration (config hostName));
  mkChoose = hostName: isNixOS: x: let
    systemType =
      if isNixOS
      then "nixos"
      else if stdenv.isDarwin
      then "darwin"
      else "linux";
    keys = ["default" systemType system hostName "${hostName}.${system}"];
  in
    concatOrUpdate (builtins.map
      (y: x."${y}" or null)
      keys);

  mkWrapGL = choose: name: package:
    if
      (choose {
        default = true;
        nixos = false;
        darwin = false;
      })
    then
      pkgs.writeShellScriptBin name ''
        exec '${pkgs.nixgl.nixGLIntel}/bin/nixGLIntel' '${package}/bin/${name}' "$@"
      ''
    else package;
}
