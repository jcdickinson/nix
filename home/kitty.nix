{
  pkgs,
  pkgs-unstable,
  choose,
  config,
  wrapGL,
  ...
}: let
  kitty = wrapGL "kitty" pkgs-unstable.kitty;
in {
  dconf.settings = choose rec {
    default = {};
    linux = {
      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
        binding = "<Super>t";
        command = "${kitty}/bin/kitty";
        name = "Open Terminal";
      };
      "org/gnome/settings-daemon/plugins/media-keys" = {
        custom-keybindings = [
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
        ];
      };
      "org/gnome/desktop/applications/terminal" = {
        exec = "${kitty}/bin/kitty";
      };
    };
    nixos = linux;
  };

  programs.kitty = {
    enable = true;
    package = kitty;
    font = {
      name = "MonoLisa Regular Nerd Font Complete";
      size = 12;
    };
    settings = {
      bold_font = "MonoLisa Bold Nerd Font Complete";
      italic_font = "MonoLisa Regular Italic Nerd Font Complete";
      bold_italic_font = "MonoLisa Bold Italic Nerd Font Complete";

      scrollback_lines = 2000;
      show_hyperlink_targets = true;
      repaint_delay = 6;
      enable_audio_bell = false;
      shell = "${pkgs-unstable.nushell}/bin/nu -i -l";
      close_on_child_death = true;
      visual_bell_duration = "0.5";
      window_padding_width = 2;

      hide_window_decorations = true;
      tab_bar_min_tabs = 1;
      tab_bar_edge = "bottom";
      tab_bar_style = "powerline";
      tab_powerline_style = "slanted";
      tab_title_template = "{title}{' :{}:'.format(num_windows) if num_windows > 1 else ''}";
      foreground = "#CDD6F4";
      background = "#1E1E2E";
      selection_foreground = "#1E1E2E";
      selection_background = "#F5E0DC";
      cursor = "#F5E0DC";
      cursor_text_color = "#1E1E2E";
      url_color = "#F5E0DC";
      active_border_color = "#B4BEFE";
      inactive_border_color = "#6C7086";
      bell_border_color = "#F9E2AF";
      active_tab_foreground = "#11111B";
      active_tab_background = "#CBA6F7";
      inactive_tab_foreground = "#CDD6F4";
      inactive_tab_background = "#181825";
      tab_bar_background = "#11111B";
      mark1_foreground = "#1E1E2E";
      mark1_background = "#B4BEFE";
      mark2_foreground = "#1E1E2E";
      mark2_background = "#CBA6F7";
      mark3_foreground = "#1E1E2E";
      mark3_background = "#74C7EC";
      color0 = "#45475A";
      color8 = "#585B70";
      color1 = "#F38BA8";
      color9 = "#F38BA8";
      color2 = "#A6E3A1";
      color10 = "#A6E3A1";
      color3 = "#F9E2AF";
      color11 = "#F9E2AF";
      color4 = "#89B4FA";
      color12 = "#89B4FA";
      color5 = "#F5C2E7";
      color13 = "#F5C2E7";
      color6 = "#94E2D5";
      color14 = "#94E2D5";
      color7 = "#BAC2DE";
      color15 = "#A6ADC8";
    };

    keybindings = {
      "shift+alt+j" = "resize_window shorter";
      "shift+alt+k" = "resize_window shorter";
      "shift+alt+h" = "resize_window narrower";
      "shift+alt+l" = "resize_window taller";
      "shift+alt+period" = "next_tab";
      "shift+alt+comma" = "previous_tab";
      "ctrl+alt+j" = "neighboring_window bottom";
      "ctrl+alt+k" = "neighboring_window top";
      "ctrl+alt+h" = "neighboring_window left";
      "ctrl+alt+l" = "neighboring_window right";
      "ctrl+alt+q" = "close_window";
      "shift+alt+enter" = "new_tab";
    };

    extraConfig = ''
      enabled_layouts fat:bias=70;full_size=1;mirrored=false
    '';
  };
}
