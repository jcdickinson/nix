{
  pkgs,
  pkgs-unstable,
  config,
  choose,
  ...
}: let
  osvars = choose {
    default = {
      profileEnv = "/etc/bash.bashrc";
    };
    "aarch64-darwin" = {
      profileEnv = "/etc/static/zshenv";
    };
  };
  shellAliases = choose rec {
    default = {
      ls = "${pkgs.exa}/bin/exa";
      lsa = "${pkgs.exa}/bin/exa -a -I '.direnv|.git' --icons --color=auto --group-directories-first --git";
      lsd = "${pkgs.exa}/bin/exa -a -I '.direnv|.git' -D -1 -T --color=auto --group-directories-first --git";
      bash = "${pkgs-unstable.bashInteractive}/bin/bash";
    };
    jono-desktop = {
      logs = "${pkgs-unstable.opentelemetry-collector-contrib}/bin/otelcontribcol --config=${files/otel/local.yaml}";
    };
    cmgx-ubt-jdickinson = {
      stactl = "${config.home.homeDirectory}/.local-docker/run.sh";
    };
    cmgx-osx-jdickinson = cmgx-ubt-jdickinson;
  };
in {
  programs.starship = {
    enable = true;
    package = pkgs-unstable.starship;
    enableNushellIntegration = false;
    settings = {
      command_timeout = 2000;
      palette = "catppuccin_mocha";
      character = {
        success_symbol = "[❯](bold green) ";
        error_symbol = "[❯](bold red) ";
      };
      shell = {
        fish_indicator = "🐟";
        nu_indicator = "✨";
        bash_indicator = "💩";
        unknown_indicator = " ";
        style = "cyan bold";
        disabled = false;
      };
      palettes.catppuccin_mocha = {
        rosewater = "#f5e0dc";
        flamingo = "#f2cdcd";
        pink = "#f5c2e7";
        mauve = "#cba6f7";
        red = "#f38ba8";
        maroon = "#eba0ac";
        peach = "#fab387";
        yellow = "#f9e2af";
        green = "#a6e3a1";
        teal = "#94e2d5";
        sky = "#89dceb";
        sapphire = "#74c7ec";
        blue = "#89b4fa";
        lavender = "#b4befe";
        text = "#cdd6f4";
        subtext1 = "#bac2de";
        subtext0 = "#a6adc8";
        overlay2 = "#9399b2";
        overlay1 = "#7f849c";
        overlay0 = "#6c7086";
        surface2 = "#585b70";
        surface1 = "#45475a";
        surface0 = "#313244";
        base = "#1e1e2e";
        mantle = "#181825";
        crust = "#11111b";
      };
    };
  };

  programs.nushell = {
    enable = true;
    package = pkgs-unstable.nushell;
    configFile.text = ''
      let nixpkgs = {
        starship: "${pkgs-unstable.starship}/bin/starship",
        direnv: "${pkgs.direnv}/bin/direnv"
      }
      source ${./files/nushell/config.nu}
      source ${./files/nushell/starship.nu}
      source ${./files/nushell/zoxide.nu}
    '';
    envFile.text = ''
      source ${./files/nushell/env.nu}
    '';
  };

  programs.bash = {
    inherit shellAliases;
    enable = true;
    profileExtra = ''
      export XDG_DATA_DIRS="$HOME/.nix-profile/share:$XDG_DATA_DIRS"
    '';
  };

  programs.fish = {
    inherit shellAliases;
    enable = true;
    shellInit = ''
      set fish_greeting

      # HACK: HM doesn't add nix env
      if [ -z "$__NIX_CUSTOM_SET_ENVIRONMENT_DONE" ]
          set -e __NIX_CUSTOM_SET_ENVIRONMENT_DONE 1
          set --prepend fish_function_path ${pkgs.fishPlugins.foreign-env}/share/fish/vendor_functions.d
          [ -e '${osvars.profileEnv}' ] && fenv source '${osvars.profileEnv}'
          set -e __HM_SESS_VARS_SOURCED
          fenv source ${config.home.profileDirectory}/etc/profile.d/hm-session-vars.sh
          set -e fish_function_path[1]
      end
    '';
    interactiveShellInit = ''
      set --global fish_color_normal cdd6f4
      set --global fish_color_command 89b4fa
      set --global fish_color_param f2cdcd
      set --global fish_color_keyword f38ba8
      set --global fish_color_quote a6e3a1
      set --global fish_color_redirection f5c2e7
      set --global fish_color_end fab387
      set --global fish_color_comment 7f849c
      set --global fish_color_error f38ba8
      set --global fish_color_gray 6c7086
      set --global fish_color_selection --background=313244
      set --global fish_color_search_match --background=313244
      set --global fish_color_operator f5c2e7
      set --global fish_color_escape eba0ac
      set --global fish_color_autosuggestion 6c7086
      set --global fish_color_cancel f38ba8
      set --global fish_color_cwd f9e2af
      set --global fish_color_user 94e2d5
      set --global fish_color_host 89b4fa
      set --global fish_color_host_remote a6e3a1
      set --global fish_color_status f38ba8
      set --global fish_pager_color_progress 6c7086
      set --global fish_pager_color_prefix f5c2e7
      set --global fish_pager_color_completion cdd6f4
      set --global fish_pager_color_description 6c7086

      function update_cwd_osc --on-variable PWD --description 'Notify terminals when $PWD changes'
        if status --is-command-substitution || set -q INSIDE_EMACS
          return
        end
        printf \e\]7\;file://%s%s\e\\ $hostname (string escape --style=url $PWD)
      end
      update_cwd_osc

      # custom
      function restore_fg
        fg >/dev/null 2>/dev/null
      end
      bind \ct restore_fg

      fish_vi_key_bindings
    '';
  };
}
