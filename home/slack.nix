{pkgs-unstable, ...}: {
  xdg.desktopEntries = {
    slack = with pkgs-unstable; {
      name = "Slack";
      comment = "Slack Desktop";
      genericName = "Slack Client for Linux";
      exec = "${slack}/bin/slack --ozone-platform-hint=auto --enable-features=UseOzonePlatform,WebRTCPipeWireCapturer,WaylandWindowDecorations @@u %U @@";
      icon = "${slack}/share/pixmaps/slack.png";
      type = "Application";
      startupNotify = true;
      categories = ["GNOME" "GTK" "Network" "InstantMessaging"];
      mimeType = ["x-scheme-handler/slack"];
      settings = {
        StartupWMClass = "Slack";
      };
    };
  };
}
