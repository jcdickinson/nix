{pkgs, ...}: {
  home.packages = [
    # Use cargo install instead
    # pkgs-unstable.pijul
  ];

  home.file.".config/pijul/config.toml" = {
    text = ''
      [tools]
      editor = { path = "${pkgs.neovim}/bin/nvim" }
      pager = { path = "${pkgs.bat}/bin/bat" }

      [author]
      name = "jcdickinson"
      full_name = "Jonathan Dickinson"
      email = "oss@jonathan.dickinson.id"
    '';
  };
}
