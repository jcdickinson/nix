{
  pkgs,
  lib,
  choose,
  ...
} @ inputs: let
  background = "file://${./files/wallpapers/2T3qqti-minimalist-backgrounds.png}";
  mkTuple = lib.hm.gvariant.mkTuple;
  cursorThemeName = "Yaru";
  cursorSize = 24;
in {
  home.packages = with pkgs; [
    yaru-theme
  ];

  home.file.".config/gtk-3.0/settings.ini" = {
    text = ''
      [Settings]
      gtk-application-prefer-dark-theme=1
      gtk-cursor-theme-name=${cursorThemeName}
    '';
  };

  home.file.".config/gtk-4.0/settings.ini" = {
    text = ''
      [Settings]
      gtk-application-prefer-dark-theme=1
      gtk-cursor-theme-name=${cursorThemeName}
    '';
  };

  home.sessionVariables = {
    XCURSOR_THEME = cursorThemeName;
    XCURSOR_SIZE = builtins.toString cursorSize;
  };
  dconf = {
    enable = true;
    settings = {
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        gtk-theme = "Adwaita";
        cursor-size = cursorSize;
        clock-show-weekday = true;
        cursor-theme = cursorThemeName;
        font-antialiasing = "rgba";
        font-hinting = "slight";
        icon-theme = "Nordzy-cyan-dark";
        monospace-font-name = "MonoLisa 10";
        document-font-name = "Ubuntu 11";
        font-name = "Ubuntu 11";
      };

      "desktop/ibus/panel/emoji" = {
        hotkey = [];
        unicode-hotkey = [];
      };

      "org/gnome/desktop/background" = {
        color-shading-type = "solid";
        picture-options = "zoom";
        picture-uri = background;
        picture-uri-dark = background;
        primary-color = "#000000000000";
        secondary-color = "#000000000000";
      };

      "org/gnome/desktop/screensaver" = {
        color-shading-type = "solid";
        picture-options = "zoom";
        picture-uri = background;
        primary-color = "#000000000000";
        secondary-color = "#000000000000";
      };

      "org/gnome/desktop/input-sources" = {
        per-window = false;
        sources = [
          (mkTuple ["xkb" "us"])
        ];
        xkb-options = ["caps:escape"];
      };

      "org/gnome/desktop/wm/keybindings" = {
        maximize = ["<Super>k"];
        unmaximize = ["<Super>j"];
        move-to-workspace-left = ["<Shift><Super>comma"];
        move-to-workspace-right = ["<Shift><Super>period"];
        switch-to-workspace-left = ["<Super>comma"];
        switch-to-workspace-right = ["<Super>period"];
      };

      "org/gnome/desktop/wm/preferences" = {
        button-layout = "appmenu:minimize,maximize,close";
        resize-with-right-button = true;
        titlebar-font = "Ubuntu Bold 11";
      };

      "org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "interactive";
      };

      "org/gnome/desktop/peripherals/mouse" = {
        natural-scroll = false;
      };

      "org/gnome/desktop/peripherals/touchpad" = {
        natural-scroll = false;
        two-finger-scrolling-enabled = true;
        click-method = "fingers";
      };

      "org/gnome/tweaks" = {
        show-extensions-notice = false;
      };

      "org/gnome/shell" = {
        disable-user-extensions = false;
        disabled-extensions = [
          "background-logo@fedorahosted.org"
          "ubuntu-dock@ubuntu.com"
          "ubuntu-appindicators@ubuntu.com"
          "ding@rastersoft.com"
        ];
        enabled-extensions = choose {
          default = [
            "appindicatorsupport@rgcjonas.gmail.com"
            "caffeine@patapon.info"
            "blur-my-shell@aunetx"
          ];
          jono-desktop = [
            "hidetopbar@mathieu.bidon.ca"
            "trayIconsReloaded@selfmade.pl"
          ];
        };
        favorite-apps = [
          # These conventiently silently fail if not installed.
          "firefox.desktop"
          "org.mozilla.firefox.desktop"
          "sublime_merge.desktop"
          "com.slack.Slack.desktop"
          "slack.desktop"
        ];
      };

      "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
        binding = "<Control><Alt>period";
        command = "${pkgs._1password-gui}/bin/1password --quick-access";
        name = "1Password Quick Access";
      };

      "org/gnome/settings-daemon/plugins/media-keys" = {
        custom-keybindings = [
          "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
        ];
        screensaver = ["<Super>Q"];
      };

      "org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9" = {
        "background-transparency-percent" = 10;
        "custom-command" = "fish -i -l";
        "default-size-columns" = 120;
        "default-size-rows" = 40;
        "font" = "MonoLisa Nerd Font 12";
        "use-custom-command" = true;
        "use-system-font" = false;
        "use-transparent-background" = true;
      };

      "org/gnome/shell/extensions/blur-my-shell" = {
        color-and-noise = false;
      };

      "org/hnome/extensions/blur-my-shell/applications" = {
        blur = true;
        blur-on-overview = false;
        brightness = 0.5;
        customize = true;
        enable-all = false;
        opacity = 210;
        sigma = 2;
        whitelist = ["Alacritty" "neovide"];
      };

      "org/gnome/shell/extensions/blur-my-shell/panel" = {
        blur = true;
      };

      "org/gnome/shell/extensions/hidetopbar" = {
        mouse-sensitive = false;
      };
    };
  };
}
