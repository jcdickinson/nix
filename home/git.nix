{choose, ...}: {
  programs.git = choose {
    default = {
      enable = true;
      lfs.enable = true;
      userName = "Jonathan Dickinson";
      userEmail = "oss@jonathan.dickinson.id";
      aliases = {
        p = "push --force-with-lease --force-if-includes";
      };
      ignores = [
        ".pijul/"
        ".direnv/"
        ".envrc"
      ];
      signing = {
        signByDefault = true;
        key = "EE33E8A4879C8F5D";
      };
      difftastic = {
        enable = true;
        background = "dark";
      };
      extraConfig = {
        core = {
          packedGitLimit = "512m";
          packedGitWindowSize = "512m";
          editor = "nvim --cmd 'let g:=unception_block_while_host_edits=1'";
        };
        pack = {
          deltaCacheSize = "2047m";
          packSizeLimit = "2047m";
          windowMemory = "2047m";
        };
        init = {defaultBranch = "main";};
        pull = {rebase = "true";};
        push = {autoSetupRemote = true;};
      };
    };
    cmgx-ubt-jdickinson = {
      userEmail = "jdickinson@cmgx.io";
      signing = {
        key = "93A3DCFDE14F34C0";
      };
    };
    cmgx-osx-jdickinson = {
      userEmail = "jdickinson@cmgx.io";
      signing = {
        key = "D46BBE0AE6BB128D";
      };
    };
  };
}
