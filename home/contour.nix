{
  pkgs-unstable,
  pkgs,
  choose,
  config,
  wrapGL,
  ...
}: let
  contour = wrapGL "contour" pkgs-unstable.contour;
in {
  home.packages = [
    contour
  ];

  home.file.".config/contour/contour.yml" = {
    text = builtins.toJSON {
      platform_plugin = "auto";
      renderer = {
        backend = "OpenGL";
        tile_hashtable_slots = 8192;
        tile_cache_count = 4000;
        tile_direct_mapping = true;
      };
      word_delimiters = " /\\()\"'-.,:;<>~!@#$%^&*+=[]{}~?|│";
      read_buffer_size = 16384;
      pty_buffer_size = 1048576;
      reflow_on_resize = true;
      bypass_mouse_protocol_modifier = "Shift";
      mouse_block_selection_modifier = "Meta";
      on_mouse_select = "CopyToSelectionClipboard";
      images = {
        sixel_scrolling = true;
        sixel_register_count = 4096;
        sixel_cursor_conformance = true;
        max_width = 0;
        max_height = 0;
      };
      profiles = {
        main = {
          copy_last_mark_range_offset = 0;
          wm_class = "contour";
          terminal_id = "VT525";
          terminal_size = {
            columns = 120;
            lines = 50;
          };
          history = {
            limit = 10000;
            auto_scroll_on_update = true;
            scroll_multiplier = 3;
          };
          permissions = {
            change_font = "deny";
            capture_buffer = "ask";
          };
          shell = "${pkgs.fish}/bin/fish";
          arguments = ["-i" "-l"];
          show_title_bar = false;
          scrollbar = {
            position = "hidden";
          };
          font = {
            size = 13;
            locator = "native";
            text_shaping = {
              engine = "native";
            };
            render_mode = "gray";
            strict_spacing = true;
            regular = {
              family = "MonoLisa Nerd Font";
              weight = "regular";
              slant = "normal";
            };
          };
          cursor = {
            shape = "block";
          };
          hyperlink_decoration = {
            normal = "dotted";
            hover = "underline";
          };
          colors = {
            default = {
              background = "#1E1E2E";
              foreground = "#CDD6F4";
            };

            cursor = {
              default = "#F5E0DC";
              text = "#1E1E2E";
            };

            selection = {
              foreground = "#1E1E2E";
              background = "#F5E0DC";
            };

            normal = {
              black = "#45475A";
              red = "#F38BA8";
              green = "#A6E3A1";
              yellow = "#F9E2AF";
              blue = "#89B4FA";
              magenta = "#F5C2E7";
              cyan = "#94E2D5";
              white = "#BAC2DE";
            };

            bright = {
              black = "#585B70";
              red = "#F38BA8";
              green = "#A6E3A1";
              yellow = "#F9E2AF";
              blue = "#89B4FA";
              magenta = "#F5C2E7";
              cyan = "#94E2D5";
              white = "#A6ADC8";
            };

            dim = {
              black = "#45475A";
              red = "#F38BA8";
              green = "#A6E3A1";
              yellow = "#F9E2AF";
              blue = "#89B4FA";
              magenta = "#F5C2E7";
              cyan = "#94E2D5";
              white = "#BAC2DE";
            };
          };
        };
      };

      input_mapping = [
        {
          mods = ["Control"];
          mouse = "Left";
          action = "FollowHyperlink";
        }
        {
          mods = [];
          mouse = "Middle";
          action = "PasteSelection";
        }
        {
          mods = [];
          mouse = "WheelDown";
          action = "ScrollDown";
        }
        {
          mods = [];
          mouse = "WheelUp";
          action = "ScrollUp";
        }
        {
          mods = ["Alt"];
          key = "Enter";
          action = "ToggleFullscreen";
        }
        {
          mods = ["Alt"];
          mouse = "WheelDown";
          action = "DecreaseOpacity";
        }
        {
          mods = ["Alt"];
          mouse = "WheelUp";
          action = "IncreaseOpacity";
        }
        {
          mods = ["Control" "Alt"];
          key = "S";
          action = "ScreenshotVT";
        }
        {
          mods = ["Control" "Shift"];
          key = "Plus";
          action = "IncreaseFontSize";
        }
        {
          mods = ["Control"];
          key = "0";
          action = "ResetFontSize";
        }
        {
          mods = ["Control" "Shift"];
          key = "Minus";
          action = "DecreaseFontSize";
        }
        {
          mods = ["Control" "Shift"];
          key = "_";
          action = "DecreaseFontSize";
        }
        {
          mods = ["Control" "Shift"];
          key = "N";
          action = "NewTerminal";
        }
        {
          mods = ["Control" "Shift"];
          key = "C";
          action = "CopySelection";
        }
        {
          mods = ["Control" "Shift"];
          key = "V";
          action = "PasteClipboard";
        }
        {
          mods = ["Control"];
          key = "C";
          action = "CopySelection";
          mode = "Select|Insert";
        }
        {
          mods = ["Control"];
          key = "V";
          action = "PasteClipboard";
          mode = "Select|Insert";
        }
        {
          mods = ["Control"];
          key = "V";
          action = "CancelSelection";
          mode = "Select|Insert";
        }
        {
          mods = [];
          key = "Escape";
          action = "CancelSelection";
          mode = "Select|Insert";
        }
        {
          mods = ["Control" "Shift"];
          key = "Space";
          action = "ViNormalMode";
          mode = "Insert";
        }
        {
          mods = ["Control" "Shift"];
          key = "Comma";
          action = "OpenConfiguration";
        }
        {
          mods = ["Control" "Shift"];
          key = "Q";
          action = "Quit";
        }
        {
          mods = ["Control"];
          mouse = "WheelDown";
          action = "DecreaseFontSize";
        }
        {
          mods = ["Control"];
          mouse = "WheelUp";
          action = "IncreaseFontSize";
        }
        {
          mods = ["Shift"];
          key = "DownArrow";
          action = "ScrollOneDown";
        }
        {
          mods = ["Shift"];
          key = "End";
          action = "ScrollToBottom";
        }
        {
          mods = ["Shift"];
          key = "Home";
          action = "ScrollToTop";
        }
        {
          mods = ["Shift"];
          key = "PageDown";
          action = "ScrollPageDown";
        }
        {
          mods = ["Shift"];
          key = "PageUp";
          action = "ScrollPageUp";
        }
        {
          mods = ["Shift"];
          key = "UpArrow";
          action = "ScrollOneUp";
        }
        {
          mods = ["Shift"];
          key = "{";
          action = "ScrollMarkUp";
          mode = "~Alt";
        }
        {
          mods = ["Shift"];
          key = "}";
          action = "ScrollMarkDown";
          mode = "~Alt";
        }
        {
          mods = ["Shift"];
          mouse = "WheelDown";
          action = "ScrollPageDown";
        }
        {
          mods = ["Shift"];
          mouse = "WheelUp";
          action = "ScrollPageUp";
        }
        {
          mods = ["Control"];
          key = "O";
          action = "OpenFileManager";
        }
        {
          mods = ["Shift"];
          key = "Insert";
          action = "ToggleAllKeyMaps";
        }
      ];
    };
  };
}
