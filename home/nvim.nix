{
  pkgs,
  pkgs-unstable,
  twt,
  choose,
  ...
}: let
  removeSuffix = x: y:
    builtins.foldl'
    (a: b: pkgs.lib.removeSuffix b a)
    x
    y;
  getName = pkgs.lib.strings.getName;
  trimExtra = x:
    removeSuffix (pkgs.lib.removePrefix "nvim-treesitter-" x)
    ["-bin" "-wrapper" "-grammar"];
  makeLuaList = v:
    builtins.concatStringsSep "\n" (builtins.map
      (x: "[\"${trimExtra (getName x)}\"] = \"${x}\",")
      v);
  lua-language-server = pkgs-unstable.sumneko-lua-language-server.overrideAttrs (
    previous: rec {
      version = "3.6.11";
      src = pkgs-unstable.fetchFromGitHub {
        owner = "luals";
        repo = "lua-language-server";
        rev = version;
        sha256 = "sha256-NMybClvpTLad7xnd8uPhUHmv6fvaYIKkFHsv7SSDi2M=";
        fetchSubmodules = true;
      };
    }
  );
  tree-sitter-nu = pkgs-unstable.tree-sitter.buildGrammar {
    language = "nu";
    version = "0.0.0";
    src = pkgs.fetchFromGitHub {
      owner = "LhKipp";
      repo = "tree-sitter-nu";
      rev = "3133b2f391be10698c4fd6bc3d16f94cf2cb39b2";
      hash = "sha256-YMrqpiNpyyA5QS97/iErvTboaayhox1ZiDA2sFb60iQ=";
    };
    meta.homepage = "https://github.com/LhKipp/tree-sitter-nu";
    fixupPhase = ''
      mkdir -p $out/bin
      mv -v $out/parser $out/bin/parser
      mkdir -p $out/parser
      ln -s $out/bin/parser $out/parser/nu.so
    '';
  };
  omnisharp = pkgs-unstable.writeShellScriptBin "OmniSharp" ''
    if ! DOTNET=$(command -v dotnet); then
      DOTNET=${pkgs-unstable.dotnet-sdk_7}/bin/dotnet
    fi
    $DOTNET '${pkgs-unstable.omnisharp-roslyn}/lib/omnisharp-roslyn/OmniSharp.dll' "$@"
  '';
  fhs = pkgs.buildFHSUserEnv {
    name = "fhs";
    targetPkgs = pkgs: (with pkgs; [zlib]);
    multiPkgs = pkgs: (with pkgs; [zlib]);
  };
  wrap-fhs = pkgs.writeShellScriptBin "wrap-fhs" ''
    exec '${fhs}/bin/fhs' -c 'exec "$@"' "$1" "$@"
  '';
  packages = with pkgs-unstable;
    choose {
      default = [
        cargo
        clang-tools_14
        curl
        dotnet-sdk_7
        fish
        gzip
        lazygit
        nil
        nix
        nodePackages.bash-language-server
        nodePackages.npm
        nodePackages.prettier
        nodePackages.sql-formatter
        nodePackages.svelte-language-server
        nodePackages.typescript-language-server
        nodePackages.vscode-css-languageserver-bin
        nodePackages.vscode-html-languageserver-bin
        nodePackages.vscode-json-languageserver
        nodejs-19_x
        omnisharp
        ripgrep
        rubyPackages.solargraph
        rustfmt
        rust-analyzer
        shfmt
        stylua
        lua-language-server
        taplo
        tree-sitter
        unzip
        vscode-extensions.vadimcn.vscode-lldb
      ];
      jono-desktop = [
        twt.twt
      ];
      nixos = [
        wrap-fhs
      ];
    };
  allGrammars =
    (builtins.filter pkgs.lib.attrsets.isDerivation
      pkgs-unstable.vimPlugins.nvim-treesitter.withAllGrammars.dependencies)
    ++ [
      tree-sitter-nu
    ];
in {
  home.packages = [
    pkgs-unstable.neovim
  ];
  home.file.".config/nvim" = {
    source = ./files/nvim;
    recursive = true;
  };
  home.file.".config/nvim/lua/nixpkgs/init.lua" = {
    text = ''
      return {
      ${makeLuaList packages}

      grammars = {
      ${makeLuaList allGrammars}
      }
      }
    '';
  };
}
