{pkgs-unstable, ...}: {
  programs.firefox = {
    enable = true;
    package = pkgs-unstable.firefox;
    profiles.default = {
      name = "default";
      isDefault = true;
      search = {
        default = "DuckDuckGo";
        force = true;
        engines = {
          "Rust Docs" = {
            urls = [
              {
                template = "https://docs.rs/releases/search";
                params = [
                  {
                    name = "query";
                    value = "{searchTerms}";
                  }
                ];
              }
            ];

            definedAliases = ["@rd"];
          };
          "Rust" = {
            urls = [
              {
                template = "https://doc.rust-lang.org/std/";
                params = [
                  {
                    name = "search";
                    value = "{searchTerms}";
                  }
                ];
              }
            ];

            definedAliases = ["@r"];
          };
          "Nix Packages" = {
            urls = [
              {
                template = "https://search.nixos.org/packages";
                params = [
                  {
                    name = "type";
                    value = "packages";
                  }
                  {
                    name = "query";
                    value = "{searchTerms}";
                  }
                ];
              }
            ];

            icon = "${pkgs-unstable.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
            definedAliases = ["@np"];
          };
          "Perplexity" = {
            urls = [
              {
                template = "https://www.perplexity.ai";
                params = [
                  {
                    name = "q";
                    value = "{searchTerms}";
                  }
                ];
              }
            ];

            definedAliases = ["@ai"];
          };
        };
      };
    };
  };
}
