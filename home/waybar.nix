{
  pkgs,
  pkgs-unstable,
  ...
}: {
  programs.waybar = {
    enable = true;
    package = pkgs-unstable.waybar-hyprland;
    systemd.enable = false;
    style = ./files/waybar/style.css;
    settings = {
      mainBar = {
        output = ["DP-2"];

        layer = "top";
        position = "top";

        modules-left = [
          "idle_inhibitor"
          "pulseaudio"
          "cpu"
          "memory"
          "temperature"
        ];

        modules-center = [
          "hyprland/window"
        ];

        modules-right = [
          "tray"
          "clock"
        ];

        idle_inhibitor = {
          format = "{icon}";
          format-icons = {
            activated = "";
            deactivated = "";
          };
        };
        clock = {
          tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          format-alt = "{:%Y-%m-%d}";
        };
        memory = {
          format = "{}% ";
        };
        temperature = {
          critical-threshold = 80;
          format = "{temperatureC}°C {icon}";
          format-icons = ["" "" ""];
        };
        tray = {
          icon-size = 20;
          spacing = 10;
        };
        pulseaudio = {
          format = "{volume}% {icon}";
          format-bluetooth = "{volume}% {icon}";
          format-bluetooth-muted = "{icon} {format_source}";
          format-muted = "{format_source}";
          format-source = "";
          format-source-muted = "";
          format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = ["" "" ""];
          };
          on-click = "pavucontrol";
        };
      };
    };
  };
}
