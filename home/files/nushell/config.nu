let-env config = {
  show_banner: false,
  hooks: {
    pre_prompt: [{
      code: "
        let direnv = (^$nixpkgs.direnv export json | from json)
        let direnv = if ($direnv | length) == 1 { $direnv } else { {} }
        $direnv | load-env
      "
    }]
  }
}
