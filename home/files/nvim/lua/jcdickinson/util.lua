local M = {}

function M.set_args(f, ...)
  local arg = { ... }
  return function()
    f(unpack(arg))
  end
end

function M.require(f, ...)
  local require_values = { ... }

  local execute_thunk
  execute_thunk = function(...)
    local resolved = {}
    local last = nil

    for _, v in ipairs(require_values) do
      local ok, dep = pcall(require, v)
      if not ok then
        print("could not load dependency: " .. dep)
        execute_thunk = function() end
        return execute_thunk()
      end
      last = dep
      resolved[v] = dep
    end

    if type(f) == "function" then
      execute_thunk = function(...)
        f(resolved, ...)
      end
    elseif type(f) == "table" then
      f[1] = last[f[1]]
      execute_thunk = M.set_args(unpack(f))
    else
      f = last[f]
      execute_thunk = M.set_args(f)
    end

    return execute_thunk(...)
  end

  return function(...)
    return execute_thunk(...)
  end
end

function M.map(modes, keys, func, desc, options)
  options = options or {}
  local opts = vim.tbl_deep_extend("force", { desc = desc, silent = true, noremap = true }, options)
  return vim.keymap.set(modes, keys, func, opts)
end

function M.lazy_map(modes, keys, func, desc, options)
  if type(func) == "string" then
    local o = func
    func = function()
      vim.cmd(o)
    end
  end
  options = options or {}
  local opts = vim.tbl_deep_extend("force", { desc = desc, silent = true, noremap = true }, options)
  opts.requires = nil
  opts.mode = modes
  opts[1] = keys
  opts[2] = func
  return opts
end

function M.require_lazy_map(modes, keys, requires, func, desc, options)
  local execute_thunk
  if type(requires) == "table" then
    execute_thunk = M.require(func, unpack(requires))
  else
    execute_thunk = M.require(func, requires)
  end

  return M.lazy_map(modes, keys, execute_thunk, desc, options)
end

function M.bufmap(buffer)
  return function(modes, keys, func, desc)
    return M.map(modes, keys, func, desc, buffer)
  end
end

function M.pkg_path(name, path)
  local nixpkgs = require("nixpkgs")

  if not nixpkgs[name] then
    return nil
  end

  return nixpkgs[name] .. "/" .. path
end

function M.pkg_bin(name, bin, optional)
  local nixpkgs = require("nixpkgs")

  if not bin then
    bin = name
  end
  if not nixpkgs[name] then
    if optional then
      return nil
    end
    return bin
  end

  return nixpkgs[name] .. "/bin/" .. bin
end

function M.pkg(options, opt_bin, opt_args)
  local nixpkgs = require("nixpkgs")

  if opt_bin then
    options = {
      bin = opt_bin,
      pkg = options,
      args = opt_args,
    }
  elseif type(options) == "string" then
    options = {
      pkg = options,
      bin = options,
    }
  end

  if options.pkg == nil and options[1] ~= nil then
    options.pkg = options[1]
  end

  if options.bin == nil then
    if options[2] ~= nil then
      options.bin = options[2]
    else
      options.bin = options.pkg
    end
  end

  local exec = nil
  local local_bin = options.local_bin or options.bin
  local args = options.args or {}

  if vim.fn.executable(local_bin) == 1 then
    local local_args = options.local_args or args
    local local_fmt = options.local_fmt or options.fmt
    if local_fmt then
      exec = local_fmt(local_bin, local_args)
    else
      exec = { local_bin, local_args }
    end

    return vim.tbl_flatten(exec)
  else
    local pkgs_bin = options.pkgs_bin or options.bin
    local pkgs_args = options.pkgs_args or options.args
    local pkgs_fmt = options.pkgs_fmt or options.fmt

    local found_bin = nixpkgs[options.pkg]
    if not found_bin then
      for k, v in pairs(nixpkgs) do
        if string.find(k, options.pkg) then
          found_bin = v
          break
        end
      end
    end

    if found_bin then
      pkgs_bin = found_bin .. "/bin/" .. pkgs_bin
      if pkgs_fmt then
        exec = pkgs_fmt(pkgs_bin, pkgs_args)
      else
        exec = { pkgs_bin, pkgs_args }
      end

      return vim.tbl_flatten(exec)
    end
  end

  if options.required then
    if options.required ~= "optional" then
      error("Not found " .. options.pkg)
    end
  else
    print("Not found " .. options.pkg)
  end
  return nil
end

local auto_groups = {}
function M.auto_group(name)
  if not auto_groups[name] then
    auto_groups[name] = vim.api.nvim_create_augroup(name, {
      clear = true,
    })
  end
  return auto_groups[name]
end

function M.autocmds(group_name, ...)
  local args = { ... }
  local group = M.auto_group(group_name)
  vim.api.nvim_clear_autocmds({
    group = group,
  })

  for _, command in ipairs(args) do
    local event = command[1]
    local callback = command[2]

    if type(callback) == "function" then
      local original_callback = callback
      callback = function(a)
        original_callback({
          id = a.id,
          event = a.event,
          group = a.group,
          match = a.match,
          buffer = a.buf,
          file = a.file,
          data = a.data,
          orig = a,
        })
      end
    end
    local opts = {
      group = group,
      callback = callback,
    }
    for k, v in pairs(command) do
      opts[k] = v
    end
    while #opts ~= 0 do
      table.remove(opts, #opts)
    end
    vim.api.nvim_create_autocmd(event, opts)
  end
end

function M.buffer_autocmds(buffer, group_name, ...)
  local args = { ... }
  for _, command in ipairs(args) do
    command.buffer = buffer
  end
  M.autocmds(group_name, unpack(args))
end

function M.yoink(obj, ...)
  local args = { ... }
  local r = {}
  for _, v in ipairs(args) do
    r[v] = obj[v]
  end
  return r
end

M.bootstrapping = not not vim.g.bootstrapping
M.bootstrapping_plugins = vim.g.bootstrapping == "plugins"
M.bootstrapping_other = vim.g.bootstrapping == "other"

return M
