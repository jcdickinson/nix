local util = require("jcdickinson.util")

local has_dap = true
local has_lsp = false
local on_attach = nil
local capabilities = nil

local M = {}

local function setup()
  if not (has_lsp and has_dap) then
    return
  end

  local rust_tools = require("rust-tools")
  local common_path = "share/vscode/extensions/vadimcn.vscode-lldb/"
  local codelldb_path = util.pkg_path("vscode-extension-vadimcn-vscode-lldb", common_path .. "adapter/codelldb")
  local liblldb_path = util.pkg_path("vscode-extension-vadimcn-vscode-lldb", common_path .. "lldb/lib/liblldb.so")

  local adapter = {
    type = "server",
    port = "${port}",
    host = "127.0.0.1",
    executable = {
      command = codelldb_path,
      args = { "--liblldb", liblldb_path, "--port", "${port}" },
    },
  }

  rust_tools.setup({
    tools = {
      inlay_hints = {
        auto = false,
      },
      hover_actions = {
        handler = "rust-tools.hover_action_handlers.builtin",
      },
    },
    server = {
      cmd = util.pkg("rust-analyzer", "rust-analyzer"),
      capabilities = capabilities,
      settings = {
        ["rust-analyzer"] = {
          check = {
            command = "clippy",
          },
        },
      },
      on_attach = function(s, bufnr)
        on_attach(s, bufnr, rust_tools)
      end,
    },
    dap = {
      adapter = adapter,
    },
  })
end

function M.setup(options)
  on_attach = options.on_attach or on_attach
  capabilities = options.capabilities or capabilities
end

function M.on_lsp_attach()
  has_lsp = true
  setup()
end

function M.on_dap_attach()
  has_dap = true
  setup()
end

return M
