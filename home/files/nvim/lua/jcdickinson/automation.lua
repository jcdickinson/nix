local util = require("jcdickinson.util")

local autoformat = true

vim.api.nvim_create_user_command("Formatting", function(opts)
  local args = opts.fargs
  if args[1] == "on" or not args[1] then
    autoformat = true
  else
    autoformat = false
  end
end, {
  nargs = 1,
  complete = function()
    return { "on", "off" }
  end,
})

util.autocmds("custom-autoformat", {
  "BufWritePre",
  function()
    if autoformat then
      vim.lsp.buf.format()
    end
  end,
})
