local lazypath = vim.fn.stdpath("cache") .. "/lazy/lazy.nvim"
local root = vim.fn.stdpath("cache") .. "/lazy/root"
local lockfile = vim.fn.stdpath("cache") .. "/lazy/lock.json"
local cache = vim.fn.stdpath("cache") .. "/lazy/cache"
local readme = vim.fn.stdpath("cache") .. "/lazy/readme"
local state = vim.fn.stdpath("cache") .. "/lazy/state.json"
local util = require("jcdickinson.util")

if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=main",
		lazypath,
	})
end

if util.bootstrapping_plugins then
	print(vim.fn.system({ "git", "-C", lazypath, "fetch", "-f", "--depth", "1", "--tags" }))
	print(vim.fn.system({ "git", "-C", lazypath, "clean", "-ffdx" }))
	print(vim.fn.system({ "git", "-C", lazypath, "checkout", "-f", "refs/tags/stable" }))
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins", {
	root = root,
	lockfile = lockfile,
	defaults = {
		lazy = vim.g.bootstrapping ~= "plugins",
	},
	dev = {
		path = "~/Code/nvim-plugins",
	},
	install = {
		missing = false,
		colorscheme = { "catppuccin" },
	},
	performance = {
		cache = {
			path = cache,
		},
	},
	readme = {
		root = readme,
	},
	state = state,
})
