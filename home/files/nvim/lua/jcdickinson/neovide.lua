vim.o.guifont = "MonoLisa Nerd Font:h11"
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_refresh_rate = 144
vim.g.neovide_cursor_vfx_mode = "pixiedust"
