local M = {}

function M.install(target, config, default, setup_config)
  local function setup_handlers(name)
    local c = config[name]
    if type(c.setup) == "function" then
      c.setup()
    else
      default(name, c)
    end
  end

  local ensure_installed = {}
  for k, v in pairs(config) do
    if v.install == false or v.cmd ~= nil then
      setup_handlers(k)
    else
      ensure_installed[#ensure_installed + 1] = k
    end
  end

  setup_config = setup_config or {}
  setup_config.ensure_installed = ensure_installed
  target.setup(setup_config)
  if target.setup_handlers then
    target.setup_handlers({
      setup_handlers,
    })
  end
end

return M
