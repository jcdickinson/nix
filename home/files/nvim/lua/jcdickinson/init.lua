local ok, err = pcall(function()
	require("jcdickinson.set")
	require("jcdickinson.remap")
end)

if not ok then
	print(err)
end

require("jcdickinson.lazy")

if vim.g.bootstrapping then
	return
end

require("jcdickinson.neovide")
require("jcdickinson.automation")
