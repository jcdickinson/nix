local util = require("jcdickinson.util")

vim.g.mapleader = " "

-- Movement
util.map("v", "J", ":m '>+1<CR>gv=gv", "Move line up")
util.map("v", "K", ":m '<-2<CR>gv=gv", "Move line down")

util.map("n", "<C-d>", "<C-d>zz", "Page [d]own")
util.map("n", "<C-u>", "<C-u>zz", "Page [u]p")
util.map("n", "n", "nzzzv", "[N]ext result")
util.map("n", "N", "Nzzzv", "Previous result")

for _, v in pairs({ "<Left>", "<Right>", "<Up>", "<Down>", "<PageUp>", "<PageDown>" }) do
  util.map({ "n", "v", "i" }, v, "<nop>", "Naughty naughty")
end

-- These are not a good time
util.map("n", "Q", "<nop>")
util.map("n", "<c-z>", "<nop>")

-- Clipboard
util.map("x", "<leader>p", '"_dP', "[P]aste without yanking")
util.map({ "n", "v" }, "<leader>d", '"_d', "[D]elete without yanking")

util.map({ "n", "v" }, "<leader>y", '"+y', "[Y]ank to clipboard")
util.map("n", "<leader>Y", '"+Y', "[Y]ank end of line to clipboard")
util.map("n", "<leader>p", '"+p', "[P]aste from clipboard")
util.map("n", "<leader>P", '"+P', "[P]aste from clipboard before")
util.map("i", "<C-S-P>", "<C-R>+", "Paste from clipboard in insert mode")

-- Windows
util.map({ "t", "n", "v", "i" }, "<A-h>", [[<Cmd>wincmd h<CR>]])
util.map({ "t", "n", "v", "i" }, "<A-j>", [[<Cmd>wincmd j<CR>]])
util.map({ "t", "n", "v", "i" }, "<A-k>", [[<Cmd>wincmd k<CR>]])
util.map({ "t", "n", "v", "i" }, "<A-l>", [[<Cmd>wincmd l<CR>]])
util.map({ "t", "n", "v", "i" }, "<A-q>", [[<Cmd>wincmd q<CR>]])
util.map({ "t", "n", "v", "i" }, "<C-A-h>", [[<Cmd>resize -5<CR>]])
util.map({ "t", "n", "v", "i" }, "<C-A-l>", [[<Cmd>resize +5<CR>]])
util.map({ "t", "n", "v", "i" }, "<C-A-j>", [[<Cmd>vertical resize -5<CR>]])
util.map({ "t", "n", "v", "i" }, "<C-A-k>", [[<Cmd>vertical resize +5<CR>]])

util.map({ "t", "n", "v", "i" }, "<A-,>", "<Cmd>tabn<CR>", "Previous tab")
util.map({ "t", "n", "v", "i" }, "<A-.>", "<Cmd>tabN<CR>", "Next tab")
util.map({ "t", "n", "v", "i" }, "<S-A-.>", "<Cmd>tabnew<CR>", "New tab")
util.map({ "t", "n", "v", "i" }, "<A-Q>", "<Cmd>tabclose<CR>", "Quit current tab")
util.map({ "t", "n", "v", "i" }, "<A-o>", "<Cmd>tabonly<CR>", "Quit all except current tab")

-- Terminal
local function set_terminal_keymaps(info)
  util.map("t", [[<C-\><C-\>]], [[<C-\><C-n>]], "Normal Mode", util.yoink(info, "buffer"))
end

util.autocmds("custom-terminal", { "TermOpen", set_terminal_keymaps, pattern = "term://*" })

-- Indentation
util.map("n", "<c-.>", ">>", "[>] Indent current line")
util.map("i", "<c-.>", "<c-t>", "[>] Indent current line")
util.map("v", "<c-.>", ">gv", "[>] Indent current line")

util.map("n", "<c-,>", "<<", "[<] Unindent current line")
util.map("i", "<c-,>", "<c-d>", "[<] Unindent current line")
util.map("v", "<c-,>", "<gv", "[<] Unindent current line")

-- LSP
util.map("n", "<leader>rr", util.set_args(vim.lsp.buf.rename), "[R]efactor [R]ename")
util.map("n", "<leader>rf", util.set_args(vim.lsp.buf.format), "[R]efector [F]ormat")
util.map("n", "<leader>ra", util.set_args(vim.lsp.buf.code_action), "[R]efactor [A]ction")

util.map("n", "<leader>gd", util.set_args(vim.lsp.buf.definition), "[G]oto [D]efinition")
util.map("n", "<leader>gD", util.set_args(vim.lsp.buf.declaration), "[G]oto [D]eclaration")
util.map("n", "<leader>gi", util.set_args(vim.lsp.buf.implementation), "[G]oto [I]mplementation")
util.map("n", "<leader>gt", util.set_args(vim.lsp.buf.type_definition), "[G]oto [T]ype Definition")

util.map("n", "K", util.set_args(vim.lsp.buf.hover), "Hover Documentation")
util.map("n", "<C-k>", util.set_args(vim.lsp.buf.signature_help), "Signature Documentation")

util.map("n", "<leader>wa", util.set_args(vim.lsp.buf.add_workspace_folder), "[W]orkspace [A]dd Folder")
util.map("n", "<leader>wr", util.set_args(vim.lsp.buf.remove_workspace_folder), "[W]orkspace [R]emove Folder")

-- LSP Diagnostics
util.map("n", "[d", util.set_args(vim.diagnostic.goto_prev), "Previous [d]iagnostic")
util.map("n", "]d", util.set_args(vim.diagnostic.goto_next), "Next [d]iagnostic")
