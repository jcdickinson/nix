local util = require("jcdickinson.util")

return {
	"numToStr/Comment.nvim",
	name = "comment",
	lazy = util.bootstrapping,
	config = function()
		require("Comment").setup({
			mappings = {
				extra = false,
			},
		})
	end,
}
