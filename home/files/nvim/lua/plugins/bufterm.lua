local util = require("jcdickinson.util")
local terminals = {}
local function open_terminal(term_number)
  local Terminal = require("bufterm.terminal").Terminal

  if not terminals[term_number] then
    terminals[term_number] = Terminal:new({
      cmd = "nu -i -l",
      buflisted = false,
      termlisted = true,
    })
  end
  local term = terminals[term_number]
  term:spawn()

  for _, tab in ipairs(vim.api.nvim_list_tabpages()) do
    for _, win in ipairs(vim.api.nvim_tabpage_list_wins(tab)) do
      if vim.api.nvim_win_get_buf(win) == term.bufnr then
        vim.api.nvim_set_current_win(win)
        return
      end
    end
  end

  vim.cmd.tabnew()
  vim.api.nvim_win_set_buf(0, term.bufnr)
end

return {
  "boltlessengineer/bufterm.nvim",
  lazy = util.bootstrapping,
  config = function()
    require("bufterm").setup({
      save_native_terms = true,
      start_in_insert = true,
      remember_mode = true,
      enable_ctrl_w = false,
      terminal = {
        buflisted = false,
        termlisted = true,
        fallback_on_exit = true,
        auto_close = true,
      },
    })

    util.autocmds("TerminalCustom", {
      "BufEnter",
      pattern = "term://*",
      function()
        vim.api.nvim_win_set_option(0, "list", false)
        vim.api.nvim_win_set_option(0, "colorcolumn", "")
        vim.api.nvim_win_set_option(0, "statuscolumn", "")
      end,
    })

    vim.api.nvim_create_user_command("OpenTerminal", function(args)
      open_terminal(args.count + 1)
    end, {
      desc = "Open terminal",
      count = true,
    })

    vim.keymap.set({ "t", "n", "v", "i" }, "<A-t>", '<Cmd>execute v:count . "OpenTerminal"<CR>', {
      desc = "Open [T]erminal",
      silent = true,
      noremap = true,
    })
  end,

  -- test
}
