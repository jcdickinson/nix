local util = require("jcdickinson.util")

if false then
  return {
    "Exafunction/codeium.vim",
    lazy = false,
  }
else
  return {
    "jcdickinson/codeium.nvim",
    name = "codeium.nvim",
    lazy = util.bootstrapping,
    -- dev = true,
    dependencies = {
      "jcdickinson/http.nvim",
      "plenary",
      "nui",
      "hrsh7th/nvim-cmp",
    },
    config = function()
      require("codeium").setup({
        tools = {
          curl = util.pkg_bin("curl"),
          gzip = util.pkg_bin("gzip"),
        },
        wrapper = util.pkg_bin("wrap-fhs", "wrap-fhs", true),
      })
    end,
  }
end
