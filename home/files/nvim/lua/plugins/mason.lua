local util = require("jcdickinson.util")

return {
	"williamboman/mason.nvim",
	name = "mason",
	lazy = util.bootstrapping_plugins,
	config = function()
		require("mason").setup({
			PATH = "skip",
			providers = {
				"mason.providers.client",
				"mason.providers.registry-api",
			},
		})
	end,
}
