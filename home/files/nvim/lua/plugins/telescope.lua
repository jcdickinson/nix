local util = require("jcdickinson.util")

local fzf = {
  "nvim-telescope/telescope-fzf-native.nvim",
  build = "make",
}

local neoclip = {
  "AckslD/nvim-neoclip.lua",
  config = function()
    require("neoclip").setup()
  end,
}

return {
  "nvim-telescope/telescope.nvim",
  lazy = util.bootstrapping,
  branch = "master",
  dependencies = {
    "plenary",
    fzf,
    neoclip,
    "nvim-telescope/telescope-file-browser.nvim",
    "nvim-telescope/telescope-ui-select.nvim",
  },
  config = function()
    local telescope = require("telescope")
    local fb_actions = telescope.extensions.file_browser.actions

    local base_arguments = {
      util.pkg("rg"),
      "--color=never",
      "--no-heading",
      "--smart-case",
      "--hidden",
      "--glob",
      "!**/.git/*",
      "--glob",
      "!**/.pijul/*",
      "--glob",
      "!**/.direnv/*",
    }

    local vimgrep_arguments = {
      base_arguments,
      "--with-filename",
      "--line-number",
      "--column",
    }

    local find_command = vim.tbl_flatten({
      base_arguments,
      "--files",
    })

    telescope.setup({
      defaults = {
        file_ignore_patterns = {
          "node_modules/.*",
          "secret.d/.*",
          "%.pem",
          ".pijul/.*",
          ".git/.*",
        },
        vimgrep_arguments = vimgrep_arguments,
        mappings = {
          i = {
            ["<C-u>"] = false,
            ["<C-d>"] = false,
          },
          n = {},
        },
      },
      extensions = {
        ["ui-select"] = {
          require("telescope.themes").get_cursor({}),
        },
        file_browser = {
          hidden = true,
          respect_gitignore = true,
          mappings = {
            i = {
              ["<A-a>"] = fb_actions.create,
              ["<A-c>"] = false,
            },
            n = {
              ["a"] = fb_actions.create,
              ["c"] = false,
            },
          },
        },
      },
      pickers = {
        find_files = {
          find_command = find_command,
        },
      },
    })

    telescope.load_extension("fzf")
    telescope.load_extension("noice")
    telescope.load_extension("file_browser")
    telescope.load_extension("ui-select")
    telescope.load_extension("neoclip")
    telescope.load_extension("notify")
  end,
  keys = {
    util.require_lazy_map("n", "<leader>?", "telescope.builtin", "oldfiles", "[?] Find recently opened files"),
    util.require_lazy_map("n", "<leader><space>", "telescope.builtin", "buffers", "[ ] Find existing buffers"),
    util.require_lazy_map(
      "n",
      "<leader>/",
      "telescope.builtin",
      "current_buffer_fuzzy_find",
      "[/] Fuzzily search in current buffer"
    ),
    util.require_lazy_map("n", "<leader>sf", "telescope.builtin", "find_files", "[S]earch [F]iles"),
    util.require_lazy_map("n", "<leader>sh", "telescope.builtin", "help_tags", "[S]earch [H]elp"),
    util.require_lazy_map("n", "<leader>sk", "telescope.builtin", "keymaps", "[S]earch [K]eys"),
    util.require_lazy_map("n", "<leader>sw", "telescope.builtin", "git_files", "[S]earch [W]orkspace"),
    util.require_lazy_map("n", "<leader>sg", "telescope.builtin", "live_grep", "[S]earch by [G]rep"),
    util.require_lazy_map("n", "<leader>ss", "telescope.builtin", "lsp_document_symbols", "[S]earch [S]ymbols"),
    util.require_lazy_map("n", "<leader>sd", "telescope.builtin", "diagnostics", "[S]earch [D]iagnostics"),

    util.require_lazy_map("n", "<leader>gr", "telescope.builtin", "lsp_references", "[G]oto [R]eferences"),
    util.require_lazy_map("n", "<leader>gci", "telescope.builtin", "lsp_incoming_calls", "[G]oto [I]ncoming [C]alls"),
    util.require_lazy_map("n", "<leader>gco", "telescope.builtin", "lsp_outgoing_calls", "[G]oto [O]utgoing [C]alls"),
    util.require_lazy_map("n", "<leader>gi", "telescope.builtin", "lsp_implementations", "[G]oto [I]mplementations"),
    util.require_lazy_map(
      "n",
      "<leader>ws",
      "telescope.builtin",
      "lsp_dynamic_workspace_symbols",
      "[W]orkspace [S]ymbols"
    ),
    util.require_lazy_map("n", "\\", "telescope", function(requires)
      local telescope = requires.telescope
      telescope.extensions.file_browser.file_browser({
        cwd = "%:p:h",
      })
    end, "[\\] Open File Tree"),
    util.require_lazy_map({ "n", "v" }, "<leader>cc", "telescope", function(requires)
      local telescope = requires.telescope
      telescope.extensions.neoclip.default()
    end, "[CC]lipboard"),
    util.require_lazy_map({ "n", "v" }, "<leader>sn", "telescope", function(requires)
      local telescope = requires.telescope
      telescope.extensions.notify.notify()
    end, "[S]earch [N]otifications"),
  },
}
