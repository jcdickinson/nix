return {
  "https://gitlab.com/HiPhish/nvim-ts-rainbow2.git",
  config = function()
    require("nvim-treesitter.configs").setup({
      rainbow = {
        enable = true,
      },
    })
  end,
  event = "LspAttach",
}
