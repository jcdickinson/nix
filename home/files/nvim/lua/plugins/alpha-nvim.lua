local util = require("jcdickinson.util")

return {
	"goolord/alpha-nvim",
	lazy = util.bootstrapping,
	config = function()
		require("alpha").setup(require("alpha.themes.dashboard").config)
	end,
}
