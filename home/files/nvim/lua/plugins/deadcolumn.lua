local util = require("jcdickinson.util")

return {
  "Bekaboo/deadcolumn.nvim",
  lazy = util.bootstrapping,
  config = function()
    require("deadcolumn").setup({
      scope = "visible",
      modes = { "i", "ic", "ix", "R", "Rc", "Rx", "Rv", "Rvc", "Rvx" },
      blending = {
        threshold = 80,
      },
      warning = {
        alpha = 0.4,
      },
    })
  end,
}
