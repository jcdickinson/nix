local util = require("jcdickinson.util")

return {
  "TimUntersberger/neogit",
  dependencies = {
    "plenary",
    "sindrets/diffview.nvim",
  },
  config = function()
    require("diffview").setup({
      enhanced_diff_hl = true,
      hooks = {
        view_opened = function(view)
          -- hide files panel
          view.panel:close()
        end,
      },
      keymaps = {
        {
          "n",
          "q",
          util.set_args(vim.cmd.tabclose),
        },
        -- disable hotkeys that may lead to another file
        { "n", "<tab>", false },
        { "n", "<s-tab>", false },
        { "n", "<leader>e", false },
        { "n", "<leader>b", false },
      },
    })
    require("neogit").setup({
      disable_hint = true,
      disable_commit_confirmation = true,
      disable_builtin_notifications = true,
      integrations = {
        diffview = true,
      },
      signs = {
        section = { "", "" },
        item = { "", "" },
        hunk = { "", "" },
      },
      mappings = {
        status = {
          ["q"] = "Close",
        },
      },
    })
  end,
  cmd = "Neogit",
  keys = {
    util.require_lazy_map("n", "<A-g>", "neogit", "open", "Open [G]it"),
  },
}
