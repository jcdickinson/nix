return {
	"kosayoda/nvim-lightbulb",
	name = "nvim-lightbulb",
	dependencies = { "antoinemadec/FixCursorHold.nvim" },
	config = function()
		require("nvim-lightbulb").setup({ autocmd = { enabled = true } })
	end,
	event = "LspAttach",
}
