local util = require("jcdickinson.util")
local rust_config = require("jcdickinson.rust-config")

local function debugcontinue(requires)
	if vim.fn.filereadable(".vscode/launch.json") then
		requires["dap.ext.vscode"].load_launchjs()
	end
	requires["dap"].continue()
end

local function debugbreakpoint(requires)
	requires["dap"].set_breakpoint(vim.fn.input({ "Breakpoint condition: " }))
end

local function debuglog(requires)
	requires["dap"].set_breakpoint(nil, nil, vim.fn.input({ "Log point message: " }))
end

return {
	"mfussenegger/nvim-dap",
	name = "nvim-dap",
	lazy = util.bootstrapping_plugins,
	dependencies = {
		"mason",
		"jayp0521/mason-nvim-dap.nvim",
	},
	config = function()
		local lsp_util = require("jcdickinson.lsp-util")

		local mason_daps = {
			node2 = {},
		}

		lsp_util.install(require("mason-nvim-dap"), mason_daps, function(source_name)
			require("mason-nvim-dap.automatic_setup")(source_name)
		end, {
			automatic_setup = {
				adapters = {},
			},
		})
	end,

	keys = {
		util.require_lazy_map("n", "<f5>", { "dap", "dap.ext.vscode" }, debugcontinue, "Debug continue"),
		util.require_lazy_map("n", "<leader>bc", { "dap" }, debugbreakpoint, "[C]onditional [B]reakpoint"),
		util.require_lazy_map("n", "<leader>bl", { "dap" }, debuglog, "[L]ogging [B]reakpoint"),
		util.require_lazy_map("n", "<f10>", "dap", "step_over", "Step over"),
		util.require_lazy_map("n", "<f11>", "dap", "step_into", "Step into"),
		util.require_lazy_map("n", "<s-f11>", "dap", "step_out", "Step out"),
		util.require_lazy_map("n", "<leader>bt", "dap", "toggle_breakpoint", "[T]oggle [B]reakpoint"),
	},
}
