return {
  "jcdickinson/wpm.nvim",
  name = "wpm.nvim",
  lazy = true,
  config = function()
    require("wpm").setup({})
  end,
}
