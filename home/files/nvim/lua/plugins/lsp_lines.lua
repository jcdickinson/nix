local util = require("jcdickinson.util")

vim.diagnostic.config({
  virtual_lines = false,
})

local hide = function()
  return false
end

return {
  "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
  lazy = true,
  dependencies = {
    "nvim-lspconfig",
  },
  config = function()
    local lsp_lines = require("lsp_lines")
    lsp_lines.setup()
  end,
  keys = {
    util.lazy_map("n", "H", function()
      if hide() then
        return
      end

      hide = function()
        hide = function()
          return false
        end
        util.autocmds("lsplines-custom")
        vim.diagnostic.config({
          virtual_lines = false,
        })
        return true
      end

      vim.diagnostic.config({
        virtual_lines = {
          only_current_line = true,
        },
      })

      util.autocmds("lsplines-custom", {
        { "CursorMoved", "ModeChanged" },
        function()
          hide()
        end,
      })
    end, "Toggle [H]elp LSP Lines"),
  },
}
