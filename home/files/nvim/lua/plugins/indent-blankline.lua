local util = require("jcdickinson.util")

return {
	"lukas-reineke/indent-blankline.nvim",
	name = "indent-blankline",
	lazy = util.bootstrapping,
	config = function()
		require("indent_blankline").setup({
			space_char_blankline = " ",
			show_trailing_blankline_indent = false,
			char_highlight_list = {
				"IndentBlanklineIndent1",
				"IndentBlanklineIndent2",
				"IndentBlanklineIndent3",
				"IndentBlanklineIndent4",
				"IndentBlanklineIndent5",
				"IndentBlanklineIndent6",
			},
		})
	end,
}
