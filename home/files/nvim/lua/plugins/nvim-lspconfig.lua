local util = require("jcdickinson.util")
local lsp_util = require("jcdickinson.lsp-util")
local rust_config = require("jcdickinson.rust-config")

return {
  "neovim/nvim-lspconfig",
  name = "nvim-lspconfig",
  lazy = util.bootstrapping_plugins,
  dependencies = {
    "plenary",
    "nvim-lightbulb",
    "rust-tools",
    "cmp-nvim-lsp",
    "neodev",
    "mason",
    "williamboman/mason-lspconfig.nvim",
    "jose-elias-alvarez/null-ls.nvim",
  },
  config = function()
    local null_ls = require("null-ls")
    local omnisharp = require("jcdickinson.omnisharp")

    local capabilities = vim.lsp.protocol.make_client_capabilities()
    vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())
    capabilities.textDocument.completion.completionItem.snippetSupport = true
    capabilities.textDocument.completion.completionItem.resolveSupport = {
      properties = {
        "documentation",
        "detail",
        "additionalTextEdits",
      },
    }

    local null_filetypes = {}

    local function on_attach(client, bufnr, rt)
      if rt then
        util.map("n", "<leader>rh", util.set_args(rt.hover_actions.hover_actions), "Hover actions", {
          buffer = bufnr,
        })
      end

      omnisharp.fix_client(client)

      local filetypes = client.config.filetypes
      for _, v in ipairs(filetypes) do
        if null_filetypes[v] then
          client.server_capabilities.documentRangeFormattingProvider = false
          client.server_capabilities.documentFormattingProvider = false
          break
        end
      end

      if client.server then
        local caps = client.server.capabilities
        if caps.documentHighlightProvider then
          util.buffer_autocmds(
            bufnr,
            "custom-lsp-highlight-provider",
            { "CursorHold", util.set_args(vim.lsp.buf.document_highlight) },
            { "CursorMoved", util.set_args(vim.lsp.buf.clear_references) }
          )
        end

        if caps.semanticTokensProvider then
          print("Semantic")
          util.buffer_autocmds(
            bufnr,
            "custom-lsp-semantic",
            { "BufEnter,CursorHold,InsertLeave", util.set_args(vim.lsp.semantic_tokens.refresh, bufnr) }
          )
        end
      end
    end

    rust_config.setup({
      capabilities = capabilities,
      on_attach = on_attach,
    })

    local mason_servers = {
      clangd = {
        cmd = util.pkg("clang-tools", "clangd"),
      },
      lua_ls = {
        cmd = util.pkg("lua-language-server", "lua-language-server"),
        before_init = function(...)
          require("neodev.lsp").before_init(...)
        end,
        settings = {
          Lua = {
            workspace = { checkThirdParty = false },
            telemetry = { enable = false },
          },
        },
      },
      nil_ls = {
        cmd = util.pkg("nil"),
        settings = {
          ["nil"] = {
            formatting = {
              command = { "nix", "fmt", "--", "--quiet", "-" },
            },
          },
        },
      },
      omnisharp = {
        cmd = util.pkg("OmniSharp"),
        enable_roslyn_analyzers = true,
        organize_imports_on_format = true,
        enable_import_completion = true,
        analyze_open_documents_only = true,
      },
      svelte = {
        cmd = util.pkg("svelte-language-server", "svelteserver", { "--stdio" }),
      },
      bashls = {
        cmd = util.pkg("bash-language-server", "bash-language-server", { "start" }),
      },
      taplo = {
        cmd = util.pkg("taplo", "taplo", { "lsp", "stdio" }),
      },
      jsonls = {
        cmd = util.pkg("vscode-json-languageserver", "vscode-json-languageserver", { "--stdio" }),
      },
      tsserver = {
        cmd = util.pkg("typescript-language-server", "typescript-language-server", { "--stdio" }),
      },
      html = {
        cmd = util.pkg("vscode-html-languageserver", "html-languageserver", { "--stdio" }),
      },
      cssls = {
        cmd = util.pkg("vscode-css-languageserver", "css-languageserver", { "--stdio" }),
      },
      rust_analyzer = {
        install = false,
        setup = rust_config.on_lsp_attach,
      },
      solargraph = {
        cmd = util.pkg("solargraph", "solargraph", { "stdio" }),
      },
    }

    local null_sources = {
      null_ls.builtins.formatting.prettier.with({
        command = util.pkg("prettier"),
      }),
      null_ls.builtins.formatting.stylua.with({
        command = util.pkg("stylua"),
      }),
      null_ls.builtins.formatting.sql_formatter.with({
        command = util.pkg("sql-formatter"),
      }),
    }

    for _, v in ipairs(null_sources) do
      for _, f in ipairs(v.filetypes) do
        null_filetypes[f] = true
      end
    end

    null_ls.setup({
      sources = null_sources,
    })

    local mason_spawn = require("mason-core.spawn")
    mason_spawn._aliases.cargo = util.pkg_bin("cargo")
    mason_spawn._aliases.npm = util.pkg_bin("npm")
    mason_spawn._aliases.node = util.pkg_bin("node")
    mason_spawn._aliases.unzip = util.pkg_bin("unzip")

    lsp_util.install(require("mason-lspconfig"), mason_servers, function(server_name, config)
      local lsp = require("lspconfig")

      local auto = lsp[server_name]
      config.on_attach = on_attach
      config.capabilities = capabilities
      config.install = nil
      auto.setup(config)
    end)
  end,
}
