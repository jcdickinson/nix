local utils = require("jcdickinson.util")

return {
	"AckslD/nvim-neoclip.lua",
	lazy = utils.bootstrapping,
	dependencies = {
		"nvim-telescope/telescope.nvim",
	},
	config = function()
		require("neoclip").setup()
	end,
}
