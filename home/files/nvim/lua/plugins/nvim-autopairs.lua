local util = require("jcdickinson.util")

return {
	"windwp/nvim-autopairs",
	name = "nvim-autopairs",
	lazy = util.bootstrapping,
	config = function()
		require("nvim-autopairs").setup({
			check_ts = true,
		})

		local cmp_autopairs = require("nvim-autopairs.completion.cmp")
		local cmp = require("cmp")
		cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
	end,
}
