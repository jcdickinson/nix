local util = require("jcdickinson.util")

return {
  "jcdickinson/http.nvim",
  lazy = not util.bootstrapping,
  build = "cargo build --workspace --release",
  -- dev = true,
}
