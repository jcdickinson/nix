return {
	"folke/neodev.nvim",
	name = "neodev",
	lazy = true,
	dependencies = { "neoconf" },
	config = function()
		require("neodev").setup({
			setup_jsonls = false,
			lspconfig = false,
		})
	end,
}
