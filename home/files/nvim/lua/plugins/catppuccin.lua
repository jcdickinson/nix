local util = require("jcdickinson.util")

return {
  "catppuccin/nvim",
  name = "catppuccin",
  lazy = util.bootstrapping,
  priority = 1000,
  config = function()
    require("catppuccin").setup({
      flavour = "mocha",
      -- transparent_background = true,
      term_colors = true,
      integrations = {
        cmp = true,
        neotree = true,
        noice = true,
        dap = {
          enabled = true,
          enable_ui = true,
        },
        native_lsp = {
          enabled = true,
        },
        treesitter = true,
        ts_rainbow = true,
        indent_blankline = {
          enabled = true,
          colored_indent_levels = true,
        },
        telescope = true,
        notify = true,
        mini = true,
        barbar = true,
        neogit = true,
        semantic_tokens = true,
        gitsigns = true,
      },
    })

    vim.cmd([[
highlight link DiffAdd TelescopeResultsDiffAdd 
highlight link DiffDelete TelescopeResultsDiffDelete 
highlight link DiffChange TelescopeResultsDiffChange 
highlight DiffText  NONE
highlight NonText NONE
highlight link NonText IndentBlanklineSpaceChar
]])

    vim.cmd.colorscheme("catppuccin")
  end,
}
