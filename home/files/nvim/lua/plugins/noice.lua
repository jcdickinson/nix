local util = require("jcdickinson.util")

return {
  "folke/noice.nvim",
  lazy = util.bootstrapping,
  dependencies = { "nui", "rcarriga/nvim-notify" },
  config = function()
    local noice_lsp = require("noice.lsp")

    require("notify.config").setup({
      render = "compact",
      max_width = 60,
    })

    require("noice").setup({
      lsp = {
        override = {
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
          ["cmp.entry.get_documentation"] = true,
        },
        message = {
          view = "mini",
        },
      },
      presets = {
        bottom_search = true,
        command_palette = true,
        long_message_to_split = true,
        inc_rename = true,
        lsp_doc_border = false,
      },
      popupmenu = {
        enabled = true,
        backend = "nui",
      },
      messages = {
        view = false,
      },
      views = {
        notify = {
          render = "compact",
        },
      },
      routes = {
        {
          filter = { event = "notify", find = "Format request failed" },
          skip = true,
        },
        {
          filter = { event = "notify", min_height = 5 },
          view = "messages",
        },
      },
    })

    util.map("n", "<c-d>", function()
      if not noice_lsp.scroll(4) then
        return "<c-d>"
      end
    end, "Scroll Buffer or LSP Info [D]own")

    util.map("n", "<c-u>", function()
      if not noice_lsp.scroll(-4) then
        return "<c-u>"
      end
    end, "Scroll Buffer or LSP Info [U]p")
  end,
}
