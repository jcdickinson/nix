local util = require("jcdickinson.util")

return {
	"folke/persistence.nvim",
	config = function()
		require("persistence").setup()
	end,
	event = "BufReadPre", -- this will only start session saving when an actual file was opened
	keys = {
		util.require_lazy_map("n", "<leader>wl", "persistence", "load", "[W]orkspace [Load]"),
	},
}
