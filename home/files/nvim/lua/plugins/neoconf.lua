return {
	"folke/neoconf.nvim",
	name = "neoconf",
	lazy = true,
	config = function()
		require("neoconf").setup({
			plugins = {
				jsonls = {
					enabled = false,
				},
			},
		})
	end,
}
