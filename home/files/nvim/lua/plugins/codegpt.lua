local util = require("jcdickinson.util")

return {
  "dpayne/CodeGPT.nvim",
  lazy = util.bootstrapping,
  dependencies = {
    "plenary",
    "nui",
  },
  config = function()
    require("codegpt.config")

    vim.g.codegpt_commands = {
      test = {
        user_message_template = "I have the following {{language}} code: ```{{filetype}}\n{{text_selection}}```\nWrite really good unit tests using best practices for the given language. {{command_args}}. {{language_instructions}} Only return the unit tests. Only return the code snippet and nothing else. ",
        callback_type = "code_popup",
        language_instructions = {
          rust = "I already have 'mod tests', omit it. Tests should begin with the struct and method name (in snake_case) at minimum, without the 'test_' prefix.",
        },
      },
    }
  end,
}
