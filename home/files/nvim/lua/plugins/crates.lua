local util = require("jcdickinson.util")

return {
	"saecki/crates.nvim",
	name = "crates",
	tag = "v0.3.0",
	lazy = true,
	dependencies = { "plenary" },
	config = function()
		local crates = require("crates")

		crates.setup()
		require("cmp").setup({ sources = { { name = "crates" } } })

		util.map("n", "<leader>ct", crates.toggle, "[C]rates [T]oggle")
		util.map("n", "<leader>cr", crates.reload, "[C]rates [R]eload")

		util.map("n", "<leader>cv", crates.show_versions_popup, "[C]rates show [V]ersions popup")
		util.map("n", "<leader>cf", crates.show_features_popup, "[C]rates show [F]eatures popup")
		util.map("n", "<leader>cd", crates.show_dependencies_popup, "[C]rates show [D]ependencies popup")

		util.map({ "n", "v" }, "<leader>cu", crates.update_crate, "[C]rates [U]pdate crate")
		util.map("n", "<leader>ca", crates.update_all_crates, "[C]rates update [A]ll crates")
		util.map({ "n", "v" }, "<leader>cU", crates.upgrade_crate, "[C]rates [U]pgrade crate")
		util.map("n", "<leader>cA", crates.upgrade_all_crates, "[C]rates upgrade [A]ll crates")

		util.map("n", "<leader>cH", crates.open_homepage, "[C]rates open [H]ome page")
		util.map("n", "<leader>cR", crates.open_repository, "[C]rates open [R]epository")
		util.map("n", "<leader>cD", crates.open_documentation, "[C]rates open [D]ocumentation")
		util.map("n", "<leader>cC", crates.open_crates_io, "[C]rates open [C]rates.io")
	end,
	event = "BufEnter Cargo.toml",
}
