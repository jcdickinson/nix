local util = require("jcdickinson.util")

return {
  "LhKipp/nvim-nu",
  lazy = util.bootstrapping,
  config = function()
    require("nu").setup({})
  end,
}
