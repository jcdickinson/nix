local util = require("jcdickinson.util")

return {
  "nvim-lualine/lualine.nvim",
  name = "lualine",
  lazy = util.bootstrapping,
  dependencies = { "nvim-web-devicons", "wpm.nvim" },
  config = function()
    local wpm = require("wpm")
    local ts = require("nvim-treesitter.statusline")

    require("lualine").setup({
      options = {
        theme = "catppuccin",
        component_separators = "|",
        section_separators = { left = "", right = "" },
      },
      sections = {
        lualine_a = {
          { "mode", separator = { left = "" }, right_padding = 2 },
        },
        lualine_b = { "filename", "branch" },
        lualine_c = { ts.statusline },
        lualine_x = { { "diagnostics", always_visible = true } },
        lualine_y = { "filetype", "progress", wpm.wpm, wpm.historic_graph },
        lualine_z = {
          { "location", separator = { right = "" }, left_padding = 2 },
        },
      },
      inactive_sections = {
        lualine_a = { "filename" },
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = { "location" },
      },
      tabline = {},
      extensions = {},
    })
  end,
}
