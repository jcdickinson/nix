
return {
	"L3MON4D3/LuaSnip",
	name = "luasnip",
	lazy = true,
	config = function()
    require("luasnip").setup({
      history = false,
      region_check_events = { "CursorMoved", "CursorHold", "InsertEnter" },
      delete_check_events= { "TextChanged" }
    })
  end
}
