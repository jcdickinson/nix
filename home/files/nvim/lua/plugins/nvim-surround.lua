return {
	"kylechui/nvim-surround",
	config = function()
		require("nvim-surround").setup({
			keymaps = {
				insert = "<C-g>s",
				insert_line = "<C-g>S",
				normal = "<leader>s",
				normal_cur = "<leader>ss",
				normal_line = "<leader>S",
				normal_cur_line = "<leader>SS",
				visual = "<leader>s",
				visual_line = "<leader>S",
				delete = "<leader>ds",
				change = "<leader>cs",
			},
		})
	end,
	keys = {
		{ "<C-g>s", mode = "i", desc = "[S]urround" },
		{ "<C-g>S", mode = "i", desc = "[S]urround Line" },
		{ "<leader>s", mode = { "v", "n" }, desc = "[S]urround" },
		{ "<leader>ss", mode = { "v", "n" }, desc = "[S]urround Current" },
		{ "<leader>S", mode = { "v", "n" }, desc = "[S]urround Line" },
		{ "<leader>SS", mode = "n", desc = "[S]urround Current Line" },
		{ "<leader>ds", mode = { "v", "n" }, desc = "[D]elete [S]urround" },
		{ "<leader>cs", mode = { "v", "n" }, desc = "[C]hange [S]urround" },
	},
}
