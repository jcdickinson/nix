local util = require("jcdickinson.util")

return {
	"ggandor/leap.nvim",
	lazy = util.bootstrapping,
	dependencies = {
		"tpope/vim-repeat",
	},
	config = function()
		require("leap").add_default_mappings()
	end,
}
