{
  pkgs,
  config,
  ...
}: {
  imports = [
    ./waybar.nix
  ];

  services.gnome-keyring = {
    enable = true;
  };

  home.file.".config/hypr/hyprpaper.conf" = let
    background = "${./files/wallpapers/2T3qqti-minimalist-backgrounds.png}";
  in {
    text = ''
      preload = ${background}
      wallpaper = DP-2,${background}
      wallpaper = HDMI-A-1,${background}
    '';
  };
  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = let
      polkit = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      waybar = "${config.programs.waybar.package}/bin/waybar";
      hyprpaper = "${pkgs.hyprpaper}/bin/hyprpaper";
      wofi = "${pkgs.wofi}/bin/wofi";
      mako = "${pkgs.mako}/bin/mako";
      terminal = config.dconf.settings."org/gnome/desktop/applications/terminal".exec;
      slurp = "${pkgs.slurp}/bin/slurp";
      grim = "${pkgs.grim}/bin/grim";
      yoink = "${pkgs.writeShellScriptBin "yoink" ''
        mkdir -p "$HOME/Pictures/Screenshots"
        exec ${grim} "$@" -t png "$HOME/Pictures/Screenshots/$(date +%Y-%m-%d_%H-%m-%s).png"
      ''}/bin/yoink";
      yoink-box = "${pkgs.writeShellScriptBin "yoink-box" ''
        exec ${yoink} -g "$(${slurp})"
      ''}/bin/yoink-box";
    in ''
      monitor=DP-2,2560x1440@144,1920x0,1
      monitor=HDMI-A-1,1920x1080@120,0x360,1

      exec-once = ${polkit}
      exec-once = ${waybar}
      exec-once = ${hyprpaper}
      exec-once = ${mako}
      exec-once = hyprctl setcursor ${config.home.sessionVariables.XCURSOR_THEME} ${config.home.sessionVariables.XCURSOR_SIZE}

      # Source a file (multi-file configs)
      # source = ~/.config/hypr/myColors.conf

      input {
          kb_layout = us
          kb_variant =
          kb_model =
          kb_options = caps:escape
          kb_rules =

          follow_mouse = 2

          touchpad {
              disable_while_typing = yes
              clickfinger_behavior=yes
              natural_scroll = no
          }

          sensitivity = 0
      }

      general {
          gaps_in = 5
          gaps_out = 5
          border_size = 2
          col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
          col.inactive_border = rgba(595959aa)
          layout = dwindle
          max_fps = 144

          col.active_border=0xffcba6f7 0xfffab387 45deg
          col.inactive_border=0xff6c7086 0xff1e1e2e 45deg
      }

      misc {
        disable_hyprland_logo = yes
        # VRR needs to be disabled in desktop mode because the kernel doesn't
        # slew rapid changes, resulting in flickering.
        vrr=2
        vfr=true
      }

      decoration {
          rounding = 10
          blur = yes
          blur_size = 3
          blur_passes = 1
          blur_new_optimizations = on

          drop_shadow = yes
          shadow_range = 10
          shadow_render_power = 2
          col.shadow = 0xdd000000

          inactive_opacity = 0.7
      }

      animations {
          enabled = yes
          bezier = myBezier, 0.05, 0.9, 0.1, 1.05
          animation = windows, 1, 7, myBezier
          animation = windowsOut, 1, 7, default, popin 80%
          animation = border, 1, 10, default
          animation = fade, 1, 7, default
          animation = workspaces, 1, 6, default
      }

      dwindle {
          pseudotile = yes
          preserve_split = yes
      }

      master {
          new_is_master = true
      }

      gestures {
          workspace_swipe = off
      }

      # Example windowrule v1
      # windowrule = float, ^(kitty)$
      # Example windowrule v2
      # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
      # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
      windowrule = tile, ^(neovide)$
      windowrule = float, title:^(Firefox — Sharing Indicator)$
      windowrule = nofocus, title:^(Firefox — Sharing Indicator)$
      windowrule = noshadow, title:^(Firefox — Sharing Indicator)$
      windowrule = opacity 0.2 override 0.2 override, title:^(Firefox — Sharing Indicator)$
      windowrule = move 50% 0, title:^(Firefox — Sharing Indicator)$
      windowrule = size 64 24, title:^(Firefox — Sharing Indicator)$

      $mainMod = SUPER

      bind = $mainMod, T, exec, ${terminal}
      bind = $mainMod, Q, killactive,
      bind = $mainMod, M, exit,
      bind = $mainMod, E, exec, dolphin
      bind = $mainMod, V, togglefloating,
      bind = $mainMod, R, exec, ${wofi} --show=drun --style=${./files/wofi/style.css} --matching=fuzzy --allow-images -I --location=top --no-actions
      bind = $mainMod, P, pseudo, # dwindle
      bind = $mainMod, O, togglesplit, # dwindle
      bind = , Print, exec, ${yoink}
      bind = ALT, Print, exec, ${yoink-box}

      bind = $mainMod, h, movefocus, l
      bind = $mainMod, l, movefocus, r
      bind = $mainMod, k, movefocus, u
      bind = $mainMod, j, movefocus, d

      bind = $mainMod, 1, workspace, 1
      bind = $mainMod, 2, workspace, 2
      bind = $mainMod, 3, workspace, 3
      bind = $mainMod, 4, workspace, 4
      bind = $mainMod, 5, workspace, 5
      bind = $mainMod, 6, workspace, 6
      bind = $mainMod, 7, workspace, 7
      bind = $mainMod, 8, workspace, 8
      bind = $mainMod, 9, workspace, 9
      bind = $mainMod, 0, workspace, 10

      bind = $mainMod SHIFT, 1, movetoworkspace, 1
      bind = $mainMod SHIFT, 2, movetoworkspace, 2
      bind = $mainMod SHIFT, 3, movetoworkspace, 3
      bind = $mainMod SHIFT, 4, movetoworkspace, 4
      bind = $mainMod SHIFT, 5, movetoworkspace, 5
      bind = $mainMod SHIFT, 6, movetoworkspace, 6
      bind = $mainMod SHIFT, 7, movetoworkspace, 7
      bind = $mainMod SHIFT, 8, movetoworkspace, 8
      bind = $mainMod SHIFT, 9, movetoworkspace, 9
      bind = $mainMod SHIFT, 0, movetoworkspace, 10

      bindm = $mainMod, mouse:272, movewindow
      bindm = $mainMod, mouse:273, resizewindow
    '';
  };
}
