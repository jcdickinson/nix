{
  pkgs,
  choose,
  ...
} @ inputs:
choose {
  default = {};

  "aarch64-darwin" = {
    home.file.".gnupg/gpg.conf" = {
      text = ''
        use-agent
      '';
    };
    home.file.".gnupg/gpg-agent.conf" = {
      text = ''
        pinentry-program ${pkgs.pinentry_mac.out}/Applications/pinentry-mac.app/Contents/MacOS/pinentry-mac
      '';
    };
  };

  "x86_64-linux" = {
    home.packages = with pkgs; [pinentry-gnome];

    programs.gpg = {
      enable = true;
      mutableKeys = true;
    };

    services.gpg-agent = {
      enable = true;
      pinentryFlavor = "gnome3";
      enableBashIntegration = true;
      enableFishIntegration = true;
    };
  };
}
