{
  credentials,
  choose,
  ...
}: {
  home.file.".config/twt/config.toml" = {
    text = ''
      [twitch]
      username = "jono_codes"
      channel = "jono_codes"
      server = "irc.chat.twitch.tv"
      # OAuth token that authenticates you into Twitch. Acquired through https://twitchapps.com/tmi/.
      token = "${credentials.twitch.irc}"

      [terminal]
      tick_delay = 1000
      maximum_messages = 150
      log_file = ""
      start_state = "Normal"

      [storage]
      channels = true
      mentions = true

      [filters]
      enabled = false
      reversed = false

      [frontend]
      date_shown = false
      date_format = "%a %b %e %T %Y"
      maximum_username_length = 26
      username_alignment = "right"
      palette = "pastel"
      title_shown = false
      margin = 0
      badges = true
      theme = "dark"
      username_highlight = true
      cursor_shape = "block"
      blinking_cursor = false
      inverted_scrolling = false
    '';
  };
}
