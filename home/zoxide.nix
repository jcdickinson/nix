{pkgs, ...}: {
  programs.zoxide.enable = true;
  home.packages = [pkgs.fzf];
}
