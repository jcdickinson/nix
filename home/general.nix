{
  pkgs,
  config,
  pkgs-unstable,
  pkgs-21_11,
  system,
  choose,
  credentials,
  wrapGL,
  jonopkgs,
  ...
} @ attrs: let
  ca-certificates = pkgs.writeTextFile {
    name = "ca-bundle";
    destination = "/etc/ssl/certs/ca-bundle.crt";
    text = ''
      ${builtins.readFile "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"}

      ${builtins.readFile ../files/zscaler-ca.crt}
    '';
  };
  rider = pkgs.writeTextFile {
    name = "Rider";
    destination = "/bin/rider";
    executable = "true";
    text = ''
      #!/bin/bash
      $HOME/.local/share/JetBrains/Toolbox/scripts/rider "$@" 2>/dev/null >/dev/null &
      disown $!
    '';
  };
  lutris-custom = with pkgs-unstable;
    lutris.override {
      extraLibraries = pkgs: [
        wine
        wine64
      ];
    };
in {
  imports = choose rec {
    default = [
      ./shell.nix
      ./git.nix
      ./gnupg.nix
      ./pijul.nix
      ./nvim.nix
      ./kitty.nix
      ./zoxide.nix
    ];

    linux = [
      ./dconf.nix
    ];
    nixos =
      linux
      ++ [
        ./hyprland.nix
        ./mako.nix
      ];

    jono-desktop = [
      ./firefox.nix
      ./twt.nix
    ];

    cmgx-ubt-jdickinson = [
      ./slack.nix
    ];
  };

  targets = choose {
    default = {};
    linux = {
      genericLinux.enable = true;
    };
  };

  home.packages = with pkgs;
    choose rec {
      default = [
        podman-compose
        tealdeer
        fd
        ripgrep
        rnix-lsp
        nix-prefetch-git
        git-absorb
        cachix
        taplo
        lldb
        zellij
        bat
        gnupg
        pkgs-unstable.gitkraken
        pkgs-unstable._1password-gui
        pkgs-unstable.lazygit
        pkgs-unstable.appflowy
        # pijul
      ];

      linux = [
        (wrapGL "neovide" pkgs-unstable.neovide)
        overmind
        appimage-run
        wl-clipboard
        gnomeExtensions.appindicator
        gnomeExtensions.blur-my-shell
        gnomeExtensions.hide-top-bar
        gnomeExtensions.caffeine
        gnomeExtensions.tray-icons-reloaded
        powertop
        nordzy-icon-theme
        ubuntu_font_family
        fractal
      ];
      nixos = linux;

      jono-desktop = [
        virt-manager
        pkgs-unstable.opentelemetry-collector-contrib
        docker-compose
        deluge
        psensor
        parted
        gparted
        vlc
        airshipper
        lutris-custom
        cinnamon.xviewer
        cinnamon.xreader
        cinnamon.nemo
        pavucontrol
        gnome.gnome-tweaks
        unzip
      ];

      cmgx-ubt-jdickinson = [
        jq
        docker-compose
        postgresql_14
        rider
        mkcert
        pkgs-unstable.yq
      ];
      cmgx-osx-jdickinson = cmgx-ubt-jdickinson;
      aarch64-darwin = [
        gnused
        docker
        gnutar
        colima
        lima
        zstd
        gawk
        nodejs-14_x
        pkgs-21_11.azure-cli
      ];
      "cmgx-ubt-jdickinson.x86_64-linux" = [
        azure-cli
      ];
    };

  home.sessionVariables = choose rec {
    default = {
      PAGER = "${pkgs.bat}/bin/bat";
      NIXOS_OZONE_WL = "1";
      OPENAI_API_KEY = credentials.openai.api;
    };
    cmgx-osx-jdickinson = {
      GITHUB_USERNAME = "jcdickinson";
      GH_APIKEY_PACK_RO = credentials.github.cmg.packages_read;
    };
    cmgx-ubt-jdickinson = let
      ca-bundle = "${ca-certificates}/etc/ssl/certs/ca-bundle.crt";
    in
      cmgx-osx-jdickinson
      // {
        LOCALDOCKER_AMBIENT_RUNTIME = "true";
        CURL_CA_BUNDLE = ca-bundle;
        GIT_SSL_CAINFO = ca-bundle;
        SSL_CERT_FILE = ca-bundle;
        NIX_SSL_CERT_FILE = ca-bundle;
      };
  };

  home.sessionPath = choose rec {
    default = [
      "${config.home.homeDirectory}/.cargo/bin"
      "${config.home.homeDirectory}/.local/bin"
      "${./files/scripts}"
    ];
    cmgx-ubt-jdickinson = [
      "${config.home.homeDirectory}/.dotnet/tools"
    ];
    cmgx-osx-jdickinson = cmgx-ubt-jdickinson;
  };

  home.file = choose {
    default = {
      ".tmux.conf" = {source = ./files/tmux.conf;};
      ".config/zellij/config.kdl" = {source = ./files/zellij/config.kdl;};
      ".config/zellij/layouts/default.kdl" = {source = ./files/zellij/layouts/default.kdl;};
      ".config/lazygit/config.yml" = {source = ./files/lazygit/config.yml;};
      ".local/share/fonts" = {
        source = ./files/fonts;
        recursive = true;
      };
      ".config/fontconfig/fonts.conf" = {source = ./files/fontconfig/fonts.conf;};
    };
    cmgx-ubt-jdickinson = {
      #   ".local/share/applications/slack.desktop" = {
      #     source = ./files/share/applications/slack.desktop;
      #   };
    };
  };

  programs = choose {
    default = {
      home-manager.enable = true;
      nix-index.enable = true;
      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
    };
    jono-desktop = {
      obs-studio = {
        enable = true;
        plugins = with pkgs.obs-studio-plugins; [
          obs-vkcapture
          obs-gstreamer
          input-overlay
          obs-backgroundremoval
        ];
      };
    };
  };
}
