{pkgs, ...}: {
  home.file.".config/mako/config" = {
    source = ./files/mako/config;
  };
  home.packages = [pkgs.libnotify];
}
