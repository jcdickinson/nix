#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
[ -z "${IN_FILE:-}" ] && IN_FILE="$SCRIPT_DIR/private-files.tar.gz.gpg"
SHA_FILE="$HOME/.cache/private-files.tar.gz.gpg.shasum"

function cleanup() {
  rm -rf "$HOME/.cache/private-files" 2>/dev/null
  shasum "$IN_FILE" > "$SHA_FILE"
}

if [ "${CHECK_CHANGED:-}" != "1" ] || ! shasum -c "$SHA_FILE" >/dev/null 2>&1; then
  mkdir -p "$HOME/.cache/private-files"
  gpg -d "$IN_FILE" | tar -xvzf - -C "$HOME/.cache/private-files"
  trap cleanup EXIT
else
  exit 0
fi
