#!/usr/bin/env bash

set -euo pipefail

if [[ "$(hostname)" =~ cmgx-* ]]; then
  echo "You don't want to do that on $(hostname)"
  exit 1
fi

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

function cleanup() {
  rm -rf "$HOME/.cache/private-files" 2>/dev/null
}
trap cleanup EXIT

mkdir -p "$HOME/.cache/private-files/licensed/monolisa/"
cp -f "$HOME/.local/share/fonts/MonoLisa"*.otf "$HOME/.cache/private-files/licensed/monolisa/"

mkdir -p "$HOME/.cache/private-files/gpg"
gpg -a --export-secret-keys > "$HOME/.cache/private-files/gpg/gpg.asc"
gpg --export-ownertrust > "$HOME/.cache/private-files/gpg/trustdb.txt"

mkdir -p "$HOME/.cache/private-files/ssh/"
cp -f "$HOME/.ssh/"* "$HOME/.cache/private-files/ssh/"

[ -e "$SCRIPT_DIR/pack-private-files.sh" ] && source "$SCRIPT_DIR/pack-private-files.sh"
