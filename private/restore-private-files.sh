#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
[ -e "$SCRIPT_DIR/unpack-private-files.sh" ] && source "$SCRIPT_DIR/unpack-private-files.sh"
[ -z "${HOSTNAME:-}" ] && HOSTNAME=$(hostname)

mkdir -p "$HOME/.local/share/fonts"

cp -fv "$HOME/.cache/private-files/licensed/monolisa/"* "$HOME/.local/share/fonts"

if ! [[ "$HOSTNAME" =~ cmgx-* ]]; then
  gpg --import "$HOME/.cache/private-files/gpg/gpg.asc"
  gpg --import-ownertrust <"$HOME/.cache/private-files/gpg/trustdb.txt"

  mkdir -p "$HOME/.ssh"
  cp -fv "$HOME/.cache/private-files/ssh/"* "$HOME/.ssh"

  chmod 700 "$HOME/.ssh"
  chmod 600 "$HOME/.ssh/"*
  chmod 644 "$HOME/.ssh/"*.pub
  fc-cache -f -v
fi
