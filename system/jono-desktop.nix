{
  config,
  pkgs,
  pkgs-unstable,
  ...
}: {
  imports = [
    ./hardware/jono-desktop.nix
  ];

  nix = {
    settings = {
      experimental-features = ["nix-command" "flakes"];
      auto-optimise-store = true;
      substituters = ["https://nix-community.cachix.org"];
      trusted-public-keys = ["nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="];
    };
    extraOptions = ''
      warn-dirty = false
    '';
    gc = {
      automatic = true;
      dates = "weekly";
    };
    optimise = {
      automatic = true;
      dates = ["weekly"];
    };
  };

  boot = {
    supportedFilesystems = ["ntfs"];
    loader = {
      systemd-boot.enable = true;
      timeout = 1;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };
  };

  networking.hostName = "jono-desktop";
  networking.networkmanager.enable = true;

  time = {
    timeZone = "America/New_York";
    hardwareClockInLocalTime = true;
  };

  systemd = {
    services = {
      systemd-udev-settle.enable = false;
      NetworkManager-wait-online.enable = false;
    };
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  sound.enable = true;
  hardware = {
    opengl = {
      driSupport32Bit = true;
      driSupport = true;
    };
    pulseaudio.enable = false;
    sane = {
      enable = true;
      extraBackends = [pkgs.sane-airscan];
    };
    bluetooth = {
      enable = true;
      settings.General.Enable = "Source,Sink,Media,Socket";
    };
  };

  services = {
    printing.enable = true;
    flatpak.enable = true;
    gvfs.enable = true;
    udev.packages = with pkgs; [cinnamon.cinnamon-settings-daemon];
    avahi = {
      enable = true;
      nssmdns = true;
    };
    xserver = {
      enable = true;
      layout = "us";
      displayManager.gdm = {
        enable = true;
        wayland = true;
      };
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      config.pipewire = {
        default.clock.rate = 48000;
      };
      media-session.config.bluez-monitor.rules = [
        {
          matches = [{"device.name" = "~bluez_card.*";}];
          actions = {
            "update-props" = {
              "bluez5.reconnect-profiles" = ["hfp_hf" "hsp_hs" "a2dp_sink"];
              "bluez5.msbc-support" = true;
              "bluez5.sbc-xq-support" = true;
            };
          };
        }
        {
          matches = [
            {"node.name" = "~bluez_input.*";}
            {"node.name" = "~bluez_output.*";}
          ];
          actions = {
            "node.pause-on-idle" = false;
          };
        }
      ];
    };
  };

  security = {
    rtkit.enable = true;
    pam.services.login.enableGnomeKeyring = true;
  };

  users = {
    mutableUsers = false;
    users.jono = {
      description = "Jonathan Dickinson";
      isNormalUser = true;
      hashedPassword = "$6$rLrUJHlPeUoS2qJH$VSRnRll0DwX8OPgcw/G8HuYeGx5t48kfhhjhaxmMfeNvdf4H9n9CS1lwZ90GTu8zJNtVa.TdhglTDmXc7tRFV/";
      extraGroups = ["scanner" "lp" "wheel" "networkmanager" "adbusers" "video" "libvirtd"];
    };
  };

  programs = {
    adb.enable = true;
    mtr.enable = true;
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    steam.enable = true;
    gamemode.enable = true;
    hyprland = {
      enable = true;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      git
    ];
    sessionVariables = {
      NIXOS_OZONE_WL = "1";
      MOZ_ENABLE_WAYLAND = "1";
    };
  };

  virtualisation = {
    libvirtd.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      defaultNetwork.dnsname.enable = true;
    };
  };

  system.stateVersion = "22.11";
}
