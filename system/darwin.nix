{pkgs, ...}: {
  nix = {
    settings = {
      trusted-users = ["@admin"];
    };
    useDaemon = true;
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
      warn-dirty = false
    '';
  };

  programs = {
    zsh.enable = true;
    bash.enable = true;
  };

  security.pki.certificateFiles = [
    "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
    "${./files/zscaler-ca.crt}"
  ];

  nix.configureBuildUsers = true;
  services.nix-daemon.enable = true;
  system.stateVersion = 4;
}
