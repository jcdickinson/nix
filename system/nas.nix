{pkgs, ...}: {
  imports = [
    ./hardware/nas.nix
    ./nas/samba.nix
    ./nas/jellyfin.nix
  ];

  nix = {
    settings = {
      experimental-features = ["nix-command" "flakes"];
      auto-optimise-store = true;
      substituters = ["https://nix-community.cachix.org"];
      trusted-public-keys = ["nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="];
    };
    extraOptions = ''
      warn-dirty = false
    '';
    gc = {
      automatic = true;
      dates = "weekly";
    };
    optimise = {
      automatic = true;
      dates = ["weekly"];
    };
  };

  networking = {
    hostName = "nas";
  };

  environment.systemPackages = with pkgs; [
    vim
    git
    libva-utils
  ];

  services = {
    openssh = {
      enable = true;
      passwordAuthentication = false;
    };
    hardware.argonone.enable = true;
  };

  users = {
    mutableUsers = false;
    users = {
      "pi" = {
        isNormalUser = true;
        uid = 1001;
        password = "pi";
        extraGroups = ["wheel"];
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF4m/1oecJ4nnwrimyP4s5+Btdu+a94VpDNOsO0RSq2A jonathan@dickinson.id"
        ];
      };
    };
  };

  system.stateVersion = "22.11";
}
