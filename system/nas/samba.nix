{...}: {
  services.samba-wsdd.enable = true;
  networking.firewall.allowedTCPPorts = [
    5357 # wsdd
    8384
    22000 # syncthing
  ];
  networking.firewall.allowedUDPPorts = [
    3702 # wsdd
    22000
    21027 # syncthing
  ];
  users = {
    users."shares" = {
      isSystemUser = true;
      uid = 301;
      group = "shares";
    };
    groups."shares" = {
      gid = 301;
    };
  };
  fileSystems = {
    "/var/media/external" = {
      device = "/dev/mapper/main-main";
      fsType = "btrfs";
      options = [
        "noatime"
        "rw"
        "space_cache"
        "subvolid=5"
        "subvol=/"
      ];
    };
  };
  services.samba = {
    enable = true;
    securityType = "user";
    openFirewall = true;
    extraConfig = ''
      workgroup = WORKGROUP
      server string = nas
      netbios name = nas
      security = user
      guest account = nobody
      map to guest = bad user
      max log size = 1000
      dns proxy = no
      load printers = no
      disable spoolss = yes
    '';
    shares = {
      storage = {
        path = "/var/media/external/storage";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "shares";
        "force group" = "shares";
      };
    };
  };

  boot.kernel.sysctl."net.core.rmem_max" = 2500000;
  services.syncthing = {
    enable = true;
    user = "shares";
    dataDir = "/var/lib/syncthing";
    configDir = "/var/lib/syncthing/.config";
    overrideDevices = true;
    overrideFolders = true;
    extraFlags = [
      "--no-default-folder"
    ];
    devices = {
      server-hel-dickinson = {id = "VYDU3WJ-PQZ44XQ-PL4LOKD-QRVPAQI-UYOYGN3-QDEQJU7-PPZ2NOY-6VIM6QJ";};
      server-zfs-rent = {id = "SOCPZNT-WKRTOKZ-ONQ35Q5-Z5B6HDV-SIQGUHJ-NXQ3IYC-A4SWYSG-ZRISDAZ";};
    };
    folders = {
      storage = {
        path = "/var/media/external/storage";
        devices = ["server-hel-dickinson" "server-zfs-rent"];
      };
    };
  };
}
