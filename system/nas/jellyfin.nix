{
  pkgs,
  config,
  ...
}: {
  networking.firewall.allowedTCPPorts = [
    8096
  ];
  networking.firewall.allowedUDPPorts = [
    1900
    7359
  ];
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      vaapiVdpau
      libvdpau-va-gl
    ];
  };
  users = {
    users."jellyfin" = {
      uid = 402;
      extraGroups = ["shares" "render"];
    };
  };
  services.jellyfin = {
    enable = true;
    user = "jellyfin";
    group = "shares";
    openFirewall = true;
  };
}
