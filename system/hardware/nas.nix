{...}: {
  boot.loader = {
    raspberryPi = {
      version = 4;
      enable = true;
      uboot.enable = false;
      firmwareConfig = ''
        dtoverlay=argonone,hysteresis=5
        dtparam=fantemp0=50,fantemp1=60,fantemp2=70,fanspeed0=0,fanspeed1=50,fanspeed2=100
        dtoverlay=vc4-kms-v3d-pi4
        disable_overscan=1
      '';
    };
    generic-extlinux-compatible.enable = false;
  };
  hardware.raspberry-pi."4" = {
    i2c0.enable = true;
    i2c1.enable = true;
    fkms-3d = {
      enable = true;
      cma = 512;
    };
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = ["noatime" "rw" "discard"];
    };
    "/boot" = {
      device = "/dev/disk/by-label/FIRMWARE";
      fsType = "vfat";
      options = ["noatime"];
    };
  };
}
