{
  description = "System Configuration for Jono";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    nixpkgs-21_11.url = "nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
    hyprland = {
      url = "github:hyprwm/Hyprland";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    twtpkgs = {
      url = "github:jcdickinson/twitch-tui";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    nix-darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-22.11";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    nixgl = {
      url = "github:guibou/nixGL";
      inputs = {
        nixpkgs.follows = "nixpkgs-unstable";
        flake-utils.follows = "flake-utils";
      };
    };
    nixos-hardware = {
      url = "github:NixOS/nixos-hardware/master";
    };
  };

  outputs = {
    home-manager,
    nixpkgs,
    nixpkgs-unstable,
    nixpkgs-21_11,
    flake-utils,
    nix-darwin,
    twtpkgs,
    neovim-nightly-overlay,
    nixgl,
    hyprland,
    nixos-hardware,
    ...
  } @ attrs:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs-config = {
        inherit system;
        overlays = [
          neovim-nightly-overlay.overlay
          nixgl.overlay
          hyprland.overlays.default
        ];
        config = {
          allowUnfree = true;
        };
      };
      pkgs = import nixpkgs pkgs-config;
      pkgs-unstable = import nixpkgs-unstable pkgs-config;
      pkgs-21_11 = import nixpkgs-21_11 pkgs-config;
      twt = twtpkgs.packages.${system};
      username = "jono";
      lib = pkgs.lib;
      inputs = attrs // {inherit pkgs pkgs-unstable;};
    in rec {
      formatter = pkgs.alejandra;

      packages = {
        homeUtils = import ./packages/homeUtils.nix inputs;

        homeConfigurations =
          packages.homeUtils.mkHome
          ["cmgx-osx-jdickinson" "cmgx-ubt-jdickinson" "jono-desktop"] (
            hostName: let
              choose = packages.homeUtils.mkChoose hostName (hostName == "jono-desktop");
              wrapGL = packages.homeUtils.mkWrapGL choose;
              homeDirectory = choose {
                "aarch64-darwin" = "/Users/${username}";
                "x86_64-linux" = "/home/${username}";
              };
              credentials = builtins.fromJSON (builtins.readFile "${homeDirectory}/.credentials.json");
            in {
              inherit pkgs lib;
              extraSpecialArgs = {
                inherit pkgs-unstable pkgs-21_11 choose twt credentials wrapGL hostName;
                jonopkgs = packages;
              };
              modules = [
                {
                  home = {
                    inherit homeDirectory username;
                    stateVersion = "22.11";
                  };
                }
                hyprland.homeManagerModules.default
                ./home/general.nix
              ];
            }
          );

        darwinConfigurations."cmgx-osx-jdickinson" = nix-darwin.lib.darwinSystem {
          inherit system pkgs;
          modules = [./system/darwin.nix];
        };

        nixosConfigurations = {
          "jono-desktop" = nixpkgs.lib.nixosSystem {
            inherit system pkgs;
            specialArgs = {inherit pkgs pkgs-unstable;};
            modules = [
              hyprland.nixosModules.default
              ./system/jono-desktop.nix
            ];
          };
          "nas" = nixpkgs.lib.nixosSystem {
            inherit system pkgs;
            specialArgs = {inherit pkgs pkgs-unstable;};
            modules = [
              nixos-hardware.nixosModules.raspberry-pi-4
              ./system/nas.nix
            ];
          };
        };
      };

      devShells = {
        default =
          pkgs.mkShell {packages = with pkgs; [shellcheck alejandra bundix];};
        nvim-setup = pkgs.mkShell {
          packages = with pkgs-unstable; [
            cargo
            clang
            llvmPackages_14.clang-unwrapped.lib
            gnutar
            curl
            nodePackages.npm
            pkg-config
            tree-sitter
            nodejs-16_x
            openssl
            cmake
            gnumake
            python3
          ];
        };
        python-ml = pkgs.mkShell {
          packages = with pkgs-unstable; [
            python310Packages.pip
            python310Packages.torch
          ];
        };
        cpp = pkgs.mkShell {
          packages = with pkgs-unstable; [
            cmake
            gcc
          ];
        };
        cmg-dotnet = let
          fhs = pkgs.buildFHSUserEnv {
            name = "fhs";
            targetPkgs = pkgs: (with pkgs; [zlib]);
            multiPkgs = pkgs: (with pkgs; [zlib]);
          };
          wrap-fhs = pkgs.writeShellScriptBin "wrap-fhs" ''
            exec '${fhs}/bin/fhs' -c 'exec "$@"' "$1" "$@"
          '';
        in
          pkgs.mkShell {
            DYLD_FALLBACK_LIBRARY_PATH = "${pkgs-unstable.openssl_3_0.out}/lib:$DYLD_FALLBACK_LIBRARY_PATH";
            DOTNET_ROOT = "${pkgs-unstable.dotnet-sdk}";
            ASPNETCORE_ENVIRONMENT = "development";
            DOTNET_CLI_TELEMETRY_OPTOUT = 1;
            packages = with pkgs-unstable; [
              yarn
              dotnet-sdk_7
              openssl_3_0
              dotnet-aspnetcore_7
              shellcheck
              distrobox
              zlib.dev
            ];
            shellHook = ''
              REPO_PATH=$(${pkgs.git}/bin/git rev-parse --show-toplevel)
              source $REPO_PATH/private/tools/userenv.sh
              export PATH
            '';
          };
        cmg-containers = pkgs.mkShell {
          packages = with pkgs-unstable; [
            skopeo
            buildah
            cosign
            shellcheck
          ];
        };
        cmg-ruby = pkgs.mkShell {
          packages = with pkgs-unstable; [
            ruby
            nodejs
            yarn
            redis
            rubyPackages.solargraph
            shellcheck
          ];
          shellHook = ''
            export "BUNDLE_RUBYGEMS__PKG__GITHUB__COM=$GH_APIKEY_PACK_RO"
            gem install bundler --conservative
            [ -e "config/application.yml" ] || cp -v "config/application.yml.template" "config/application.yml"
            bundle check || bundle install --jobs 32
            yarnpkg
          '';
        };
        cmg-terraform = pkgs.mkShell {
          packages = with pkgs-unstable; [
            terraform
            terragrunt
            shellcheck
          ];
        };
        cmg-github-actions = pkgs.mkShell {
          packages = with pkgs-unstable; [
            nodejs-16_x
            gh
            cosign
            skopeo
            nixpkgs-fmt
            shellcheck
          ];
          shellHook = ''
            npm install
          '';
        };
        cmg-local-docker = pkgs.mkShell {
          packages = with pkgs-unstable; [
            shellcheck
          ];
        };
      };
    });
}
